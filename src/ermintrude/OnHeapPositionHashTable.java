package ermintrude;

import java.util.ArrayList;
import java.util.Arrays;

import static ermintrude.Constants.NONE;

public final class OnHeapPositionHashTable {
    private static final long[] HASH_ENTRIES;
    private static final int NUM_ENTRIES = 1 << 26;
    private static final long HASH_ENTRY_MASK = NUM_ENTRIES - 1;
    static final int EXACT = 1;
    static final int FAIL_LOW = 2;
    static final int FAIL_HIGH = 3;
    static final Position position = new Position();

    static long hits = 0;
    static long misses = 0;
    static long writes = 0;

    static {
        HASH_ENTRIES = new long[NUM_ENTRIES * PositionHashEntry.SIZE];
        clear();
    }

    static void clear() {
        Arrays.fill(HASH_ENTRIES, 0L);
    }

    static int getHashEntryIndex(long hash) {
        return (int) (hash & HASH_ENTRY_MASK) * PositionHashEntry.SIZE;
    }

    static void put(long hash, int depth, int score, int flags, int move) {
        int slot = getHashEntryIndex(hash);
        HASH_ENTRIES[slot + PositionHashEntry.HASH_OFFSET] = hash;
        HASH_ENTRIES[slot + PositionHashEntry.SCORE_OFFSET] = (long) score << 32 | move;
        HASH_ENTRIES[slot + PositionHashEntry.PLY_OFFSET] = (long) depth << 32 | flags;
        writes++;
    }

    static Position get(long hash) {
        int hashEntryIndex = getHashEntryIndex(hash);
        if (HASH_ENTRIES[hashEntryIndex + PositionHashEntry.HASH_OFFSET] == hash) {
                //&& (HASH_ENTRIES[hashEntryIndex + PositionHashEntry.SCORE_OFFSET] >> 32) != 0) {
            hits++;
            return setPosition(hashEntryIndex);
        }
        misses++;
        return null;
    }

    static Position getExact(long hash) {
        int hashEntryIndex = getHashEntryIndex(hash);
        if (HASH_ENTRIES[hashEntryIndex + PositionHashEntry.HASH_OFFSET] == hash && ((int) (HASH_ENTRIES[hashEntryIndex + PositionHashEntry.PLY_OFFSET]) == EXACT)) {
            hits++;
            return setPosition(hashEntryIndex);
        }
        misses++;
        return null;
    }

    private static Position setPosition(int hashEntryIndex) {
        position.hash = HASH_ENTRIES[hashEntryIndex + PositionHashEntry.HASH_OFFSET];
        position.score = (int) (HASH_ENTRIES[hashEntryIndex + PositionHashEntry.SCORE_OFFSET] >> 32);
        position.move = (int) (HASH_ENTRIES[hashEntryIndex + PositionHashEntry.SCORE_OFFSET]);
        position.ply = (int) (HASH_ENTRIES[hashEntryIndex + PositionHashEntry.PLY_OFFSET] >> 32);
        position.flags = (int) (HASH_ENTRIES[hashEntryIndex + PositionHashEntry.PLY_OFFSET]);
        return position;
    }

    private static void walkPV(ArrayList<Move> pv, int size) {
        if (size == 0) {
            return;
        }
        Position p = getExact(Board.HASH_VALUE);
        if (p != null) {
            int move = MoveGenerator.validateMove(Utils.format(p.move));
            if (move != NONE) {
                Move m = new Move();
                m.move = move;
                m.piece = Board.SQUARE_HASH[(move >> 6) & 0x3F];
                Board.move(m.move);
                m.check = Board.countChecks() > 0;
                pv.add(m);
                walkPV(pv, size-1);
                Board.takeBack(m.move);
            }
        }
    }

    static ArrayList<Move> mainLine(int depth) {
        ArrayList<Move> pv = new ArrayList<>();
        try {
            walkPV(pv, depth);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pv;
    }
}
