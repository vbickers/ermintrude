package ermintrude;

import java.util.ArrayList;

import static ermintrude.Constants.NONE;

public final class OffHeapPositionHashTable {
    private static final UnsafeOffHeapMemory hashTable;
    private static final long MAX_ENTRIES = 1L << 28;
    private static final long HASH_ENTRY_MASK = MAX_ENTRIES - 1L;
    static final int EXACT = 1;
    static final int FAIL_LOW = 2;
    static final int FAIL_HIGH = 3;
    static final Position position = new Position();
    static final byte[] buffer = new byte[PositionHashEntry.SIZE_IN_BYTES];
    static long hits = 0;
    static long misses = 0;
    static long writes = 0;

    static {
        System.out.println("Hash table: " + MAX_ENTRIES * PositionHashEntry.SIZE_IN_BYTES + " bytes");
        hashTable = new UnsafeOffHeapMemory(MAX_ENTRIES * PositionHashEntry.SIZE_IN_BYTES);
        clear();
    }

    static void clear() {
       hashTable.clear();
    }

    static void close() {
        hashTable.free();
    }
    static long getHashEntryIndex(long hash) {
        return ((hash & HASH_ENTRY_MASK) * PositionHashEntry.SIZE_IN_BYTES);
    }

    static void put(long hash, int depth, int score, int flags, int move) {
        long hashEntryIndex = getHashEntryIndex(hash);
        setBytes(hash, 7);
        setBytes(((long) score << 32 | move), 15);
        setBytes(((long) depth << 32 | flags), 23);
        hashTable.put(hashEntryIndex, buffer);
        writes++;
    }

    static Position get(long hash) {
        long hashEntryIndex = getHashEntryIndex(hash);
        hashTable.get(hashEntryIndex, buffer);
        if (positionMatches(hash)) {
            hits++;
            return setPosition(hash);
        }
        misses++;
        return null;
    }

    private static Position setPosition(long hash) {

        long scoreInfo = toLong(PositionHashEntry.SCORE_OFFSET_IN_BYTES);
        long flagsInfo = toLong(PositionHashEntry.PLY_OFFSET_IN_BYTES);

        position.hash = hash;
        position.score = (int) (scoreInfo >> 32);
        position.move = (int) (scoreInfo);
        position.ply = (int) (flagsInfo >> 32);
        position.flags = (int) (flagsInfo);
        return position;
    }

    static boolean positionMatches(long expected) {
        return (toLong(PositionHashEntry.HASH_OFFSET_IN_BYTES) == expected);
    }

    static boolean positionMatchesExact(long expected) {
        long hash = toLong(PositionHashEntry.HASH_OFFSET_IN_BYTES);
        long flagsInfo = toLong(PositionHashEntry.PLY_OFFSET_IN_BYTES);
        return (hash == expected && ((int) flagsInfo) == EXACT);
    }

    private static long toLong(int offset) {
        long result = 0;
        for (int i = offset; i < offset + 8; i++) {
            result <<= 8;
            result |= (buffer[i] & 0xFF);
        }
        return result;
    }

    static void setBytes(long l, int offset) {
        for (int i = offset; i > offset-8; i--) {
            buffer[i] = (byte)(l & 0xFF);
            l >>= 8;
        }
    }


    static Position getExact(long hash) {
        long hashEntryIndex = getHashEntryIndex(hash);
        hashTable.get(hashEntryIndex, buffer);
        if (positionMatchesExact(hash)) {
            hits++;
            return setPosition(hash);
        }
        misses++;
        return null;

    }

    private static void walkPV(ArrayList<Move> pv, int size) {
        if (size == 0) {
            return;
        }
        Position p = getExact(Board.HASH_VALUE);
        if (p != null) {
            int move = MoveGenerator.validateMove(Utils.format(p.move));
            if (move != NONE) {
                Move m = new Move();
                m.move = move;
                m.piece = Board.SQUARE_HASH[(move >> 6) & 0x3F];
                Board.move(m.move);
                m.check = Board.countChecks() > 0;
                pv.add(m);
                walkPV(pv, size-1);
                Board.takeBack(m.move);
            }
        }
    }

    static ArrayList<Move> mainLine(int depth) {
        ArrayList<Move> pv = new ArrayList<>();
        try {
            walkPV(pv, depth);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pv;
    }

}
