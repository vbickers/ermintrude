package ermintrude;

import static ermintrude.Constants.*;

// TODO: need a static ermintrude.MoveGenerator class, that creates ermintrude.MoveList objects.
public final class MoveList {
    private int nextMove = 0;
    private int numMoves = 0;

    private final int[] mv_move = new int[256];
    private int hashMove = 0;

    private int killer1;

    // TODO: don't pass 'checks' around
    public void getMoves(int checks, int depth, int best_move) {

        hashMove = best_move;

        this.killer1 = Board.KILLER_MOVES[0];
        clear();

        if (checks == 0) {
            if (depth > 0)
                getNormalMoves();
            else
                getCaptures();
        } else if (checks > 1) {
            getDoubleCheckEvasions();
        } else {
            getSingleCheckEvasions();
        }
    }

    public void getNormalMoves() {
        long map = Board.playerMap();
        while (map != 0) {
            getMoves(Bits.lsbValue(map));
            map &= map-1;
        }

    }

    private void getCaptures() {
        long map = Board.playerMap();
        while (map != 0) {
            getCaptures(Bits.lsbValue(map));
            map &= map-1;
        }
    }

    private void getDoubleCheckEvasions() {
        if (Board.SIDE_TO_MOVE == WHITE) {
            addKingMoves(Board.KING_SQUARE_WHITE, MoveGenerator.getKingCheckEvasions());
        } else {
            addKingMoves(Board.KING_SQUARE_BLACK, MoveGenerator.getKingCheckEvasions());
        }
    }

    private void getSingleCheckEvasions() {
        long map = Board.playerMap();
        while (map != 0) {
            getCheckEvasions(Bits.lsbValue(map));
            map &= map-1;
        }
    }

    private void getMoves(int sq) {
        int type = Board.SQUARE_HASH[sq];
        type -= type > 5 ? 5 : -1;

        switch(type) {
            case PAWN :
                addPawnMoves(sq, MoveGenerator.getPawnMoves(sq));
                return;
            case KNIGHT :
                addKnightMoves(sq, MoveGenerator.getKnightMoves(sq, Board.SIDE_TO_MOVE));
                return;
            case BISHOP :
                addBishopMoves(sq, MoveGenerator.getBishopMoves(sq, Board.SIDE_TO_MOVE));
                return;
            case ROOK :
                addRookMoves(sq, MoveGenerator.getRookMoves(sq, Board.SIDE_TO_MOVE));
                return;
            case QUEEN :
                addQueenMoves(sq, MoveGenerator.getQueenMoves(sq, Board.SIDE_TO_MOVE));
                return;
            default:
                addKingMoves(sq, MoveGenerator.getKingMoves(Board.SIDE_TO_MOVE));
        }
    }

    private void getCaptures(int sq) {
        int type = Board.SQUARE_HASH[sq];
        type -= type > 5 ? 5 : -1;
        switch (type) {
            case PAWN:
                addPawnMoves(sq, MoveGenerator.getPawnCaptures(sq));
                return;
            case KNIGHT:
                addKnightMoves(sq, MoveGenerator.getKnightCaptures(sq, Board.SIDE_TO_MOVE));
                return;
            case BISHOP:
                addBishopMoves(sq, MoveGenerator.getBishopCaptures(sq, Board.SIDE_TO_MOVE));
                return;
            case ROOK:
                addRookMoves(sq, MoveGenerator.getRookCaptures(sq, Board.SIDE_TO_MOVE));
                return;
            case QUEEN:
                addQueenMoves(sq, MoveGenerator.getQueenCaptures(sq, Board.SIDE_TO_MOVE));
                return;
            default:
                addKingMoves(sq, MoveGenerator.getKingCaptures(Board.SIDE_TO_MOVE));
        }
    }

    private void getCheckEvasions(int sq) {
        int type = Board.SQUARE_HASH[sq];
        type -= type > 5 ? 5 : -1;
        switch (type) {
            case PAWN:
                addPawnMoves(sq, MoveGenerator.getPawnCheckEvasions(sq));
                return;
            case KNIGHT:
                addKnightMoves(sq, MoveGenerator.getKnightCheckEvasions(sq));
                return;
            case BISHOP:
                addBishopMoves(sq, MoveGenerator.getBishopCheckEvasions(sq));
                return;
            case ROOK:
                addRookMoves(sq, MoveGenerator.getRookCheckEvasions(sq));
                return;
            case QUEEN:
                addQueenMoves(sq, MoveGenerator.getQueenCheckEvasions(sq));
                return;
            default:
                addKingMoves(sq, MoveGenerator.getKingCheckEvasions());
        }
    }

    private void addKingMoves(int f, long moves) {

        while (moves != 0) {

            int t = Bits.lsbValue(moves);

            int mv = f << 6 | t;

            int score = 0;

            if (mv == hashMove) {
                score += 50;
            } else if (killer1 == mv) {
                score += 10;
            }

            if (Board.SQUARE_HASH[t] != -1) {
                mv |= MASK_CAPTURE;
                score += (32 + ((Piece.VALUE[Board.SQUARE_HASH[t]] << 6) / Piece.VALUE[Board.SQUARE_HASH[f]]));
            }

            if (mv == 0x0F3E ||    // e1g1
                mv == 0x0F3A ||    // e1c1
                mv == 0x0106 ||    // e8g8
                mv == 0x0102) { // e8c8
                    mv |= MASK_CASTLE;
                    score += 25;
            }

            mv_move[numMoves++] = mv | (score << 16);

            moves &= (moves - 1);
        }
    }

    private void addKnightMoves(int f, long moves) {

        while (moves != 0) {

            int t = Bits.lsbValue(moves);

            int mv = f << 6 | t;

            int score = 0;

            if (mv == hashMove) {
                score += 50;
            } else if (killer1 == mv) {
                score += 10;
            }

            if (Board.SQUARE_HASH[t] != -1) {
                mv |= MASK_CAPTURE;
                score += (32 + ((Piece.VALUE[Board.SQUARE_HASH[t]] << 6) / Piece.VALUE[Board.SQUARE_HASH[f]]));
            }

            // add piece-positional score
            if (Board.SIDE_TO_MOVE == BLACK) t = 63 - t;

            score += KNIGHT_POSITION_STATIC_SCORE[t];
            mv_move[numMoves++] = mv | (score << 16);

            moves &= (moves - 1);
        }
    }

    private void addBishopMoves(int f, long moves) {

        while (moves != 0) {

            int t = Bits.lsbValue(moves);

            int mv = f << 6 | t;

            int score = 0;

            if (mv == hashMove) {
                score += 50;
            } else if (killer1 == mv) {
                score += 10;
            }

            if (Board.SQUARE_HASH[t] != -1) {
                mv |= MASK_CAPTURE;
                score += (32 + ((Piece.VALUE[Board.SQUARE_HASH[t]] << 6) / Piece.VALUE[Board.SQUARE_HASH[f]]));
            }

            // add piece-positional score
            if (Board.SIDE_TO_MOVE == BLACK) t = 63 - t;

            score += BISHOP_POSITION_STATIC_SCORE[t];
            mv_move[numMoves++] = mv | (score << 16);

            moves &= (moves - 1);
        }
    }

    private void addRookMoves(int f, long moves) {

        while (moves != 0) {

            int t = Bits.lsbValue(moves);

            int mv = f << 6 | t;

            int score = 0;

            if (mv == hashMove) {
                score += 50;
            } else if (killer1 == mv) {
                score += 10;
            }

            if (Board.SQUARE_HASH[t] != -1) {
                mv |= MASK_CAPTURE;
                score += (32 + ((Piece.VALUE[Board.SQUARE_HASH[t]] << 6) / Piece.VALUE[Board.SQUARE_HASH[f]]));
            }

            // add piece-positional score
            if (Board.SIDE_TO_MOVE == BLACK) t = 63 - t;

            score += ROOK_POSITION_STATIC_SCORE[t];
            mv_move[numMoves++] = mv | (score << 16);

            moves &= (moves - 1);
        }
    }

    private void addQueenMoves(int f, long moves) {

        while (moves != 0) {

            int t = Bits.lsbValue(moves);

            int mv = f << 6 | t;

            int score = 0;

            if (mv == hashMove) {
                score += 50;
            } else if (killer1 == mv) {
                score += 10;
            }

            if (Board.SQUARE_HASH[t] != -1) {
                mv |= MASK_CAPTURE;
                score += (32 + ((Piece.VALUE[Board.SQUARE_HASH[t]] << 6) / Piece.VALUE[Board.SQUARE_HASH[f]]));
            }

            // add piece-positional score
            if (Board.SIDE_TO_MOVE == BLACK) t = 63 - t;

            score += QUEEN_POSITION_STATIC_SCORE[t];
            mv_move[numMoves++] = mv | (score << 16);

            moves &= (moves - 1);
        }
    }

    private void addPawnMoves(int f, long moves) {

        while (moves != 0) {

            int t = Bits.lsbValue(moves);

            int mv = f << 6 | t;

            int score = 0;

            if (mv == hashMove) {
                score += 50;
            } else if (killer1 == mv) {
                score += 10;
            }

            if (Board.SQUARE_HASH[t] != -1) {
                mv |= MASK_CAPTURE;
                score += (32 + ((Piece.VALUE[Board.SQUARE_HASH[t]] << 6) / Piece.VALUE[Board.SQUARE_HASH[f]]));
            }

            // handle e.p capture. note we don't set the normal capture flag on this move.
            // doing so would confuse makeMove() because the target square does not
            // contain a piece
            else if (t == Board.EN_PASSANT_SQUARE) {
                mv |= MASK_CAPTURE_EN_PASSANT;
                score += 32 + ((VALUE_PAWN << 6) / VALUE_PAWN);
            }

            int rank = RANK[t];
            if (rank == 0 || rank == 7) { // about to promote:
                score += 50;
                mv_move[numMoves++] = mv | (score << 16) | MASK_PROMOTE_KNIGHT;
                mv_move[numMoves++] = mv | (score << 16) | MASK_PROMOTE_ROOK;
                mv_move[numMoves++] = mv | (score << 16) | MASK_PROMOTE_BISHOP;
                mv |= MASK_PROMOTE_QUEEN;
            } else {
                score += PAWN_POSITION_STATIC_SCORE[t];
            }

            mv_move[numMoves++] = mv | (score << 16);

            moves &= (moves - 1);
        }
    }
    public int size() {
        return numMoves;
    }

    public void reset() {
        nextMove = 0;
    }

    public void clear() {
        numMoves = 0;
        nextMove = 0;
    }

    public int pick() {
        int currentMove = -20000 << 16;
        int currentIndex = -1;
        if (nextMove < numMoves) {
            for (int i = nextMove; i < numMoves; i++) {
                if (mv_move[i] > currentMove) {
                    currentMove = mv_move[i];
                    currentIndex = i;
                }
            }
            swap(currentIndex);
            return mv_move[nextMove++];
        } else return NONE;
    }

    private void swap(int currentIndex) {
        if (currentIndex != nextMove) {
            mv_move[currentIndex] ^= mv_move[nextMove];
            mv_move[nextMove] ^= mv_move[currentIndex];
            mv_move[currentIndex] ^= mv_move[nextMove];
        }
    }

    public String simpleList() {
        StringBuilder list = new StringBuilder("moves=" + numMoves + ": ");
        for (int i = 0; i < numMoves; i++) {
            list.append(Utils.format(mv_move[i])).append("(").append(mv_move[i] >> 16).append(") ");
        }
        return list.toString();
    }

    public int pick(int hash_move) {
        if (hash_move == NONE) return pick();
        int test = hash_move & 0x7FF;
        for (int i = nextMove; i < numMoves; i++) {
            if ((mv_move[i] & 0x7FF) == test) {
                swap(i);
                return mv_move[nextMove++];
            }
        }
        return pick();
    }
}