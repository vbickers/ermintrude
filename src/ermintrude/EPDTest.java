package ermintrude;

import java.io.DataInputStream;
import java.io.InputStream;
import java.util.StringTokenizer;

public final class EPDTest {

    public static void main(String[] args) throws Exception {

        String suite = null;
        int time = 0;
        int number = 0;

        try {
            if (args.length == 3) {
                suite = args[0];
                time = Integer.parseInt(args[1]);
                number = Integer.parseInt(args[2]);
            } else {
                throw new Exception("Incorrect syntax");
            }
        } catch (Exception e) {
            System.out.println("syntax: java EPDTest <suite> <seconds per test> <number_of_tests (-1 for all)>, e.g. 'java EPDTest ecm 5 100'");
            System.exit(0);
        }

        InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(suite + ".suite");
        assert is != null;
        DataInputStream dis = new DataInputStream(is);

        Ermintrude e = new Ermintrude(null, time);
        Book b = new Book();

        int number_tried = 0;
        int solved = 0;

        while (dis.available() != 0 && (number == -1 || number_tried < number)) {

            System.out.println("Number " + ++number_tried);
            //noinspection deprecation
            String test = dis.readLine();

            StringTokenizer st = new StringTokenizer(test, ";");
            String token = st.nextToken();
            int p = token.lastIndexOf("- bm");

            String pos = token.substring(0, p + 1);
            String moves = token.substring(p + 5);

            e.setPosition(pos);

            int mv = e.playPosition();

            StringTokenizer m = new StringTokenizer(moves, " ");
            int translated_move = 0;
            while (m.hasMoreTokens()) {
                String best_move = m.nextToken();
                translated_move = b.parseMove(best_move, e.getSideToMove() + 1);
                if ((mv & 0xFFF) == (translated_move & 0xFFF)) {
                    solved++;
                    break;
                }
            }
            System.out.println("play: " + Utils.format(mv) + ", best: " + Utils.format(translated_move) + ", solved: " + solved + "/" + number_tried);
            System.out.println("*****************************************");

        }
        dis.close();
        is.close();

        System.out.println("accuracy: " + ((solved * 100) / number_tried) + "%");

    }

}
