package ermintrude;

import static ermintrude.Constants.*;

public final class Piece  {
    public static final char[] SYMBOL = {'P', 'N', 'B', 'R', 'Q', 'K', 'p', 'n', 'b', 'r', 'q', 'k'};
    public static final String[] SHORT_FORMAT_SYMBOL = {"", "N", "B", "R", "Q", "K"};
    public static final int[] VALUE = {VALUE_PAWN, VALUE_KNIGHT, VALUE_BISHOP, VALUE_ROOK, VALUE_QUEEN, VALUE_KING, VALUE_PAWN, VALUE_KNIGHT, VALUE_BISHOP, VALUE_ROOK, VALUE_QUEEN, VALUE_KING};

    public static boolean isHomeSquare(int piece, int sq) {
        switch (piece) {
            case BLACK_PAWN:
                return (sq == a7 || sq == b7 || sq == c7 || sq == d7 || sq == e7 || sq == f7 || sq == g7 || sq == h7);
            case BLACK_KNIGHT:
                return (sq == b8 || sq == g8);
            case BLACK_BISHOP:
                return (sq == c8 || sq == f8);
            case BLACK_ROOK:
                return (sq == a8 || sq == h8);
            case BLACK_QUEEN:
                return (sq == d8);
            case BLACK_KING:
                return (sq == e8);
            case WHITE_PAWN:
                return (sq == a2 || sq == b2 || sq == c2 || sq == d2 || sq == e2 || sq == f2 || sq == g2 || sq == h2);
            case WHITE_KNIGHT:
                return (sq == b1 || sq == g1);
            case WHITE_BISHOP:
                return (sq == c1 || sq == f1);
            case WHITE_ROOK:
                return (sq == a1 || sq == h1);
            case WHITE_QUEEN:
                return (sq == d1);
            case WHITE_KING:
                return (sq == e1);
        }
        return false;
    }
}