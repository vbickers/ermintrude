package ermintrude;

import java.io.*;

/**
 * Provides console input/output capability (non-UCI)
 */
public final class CLI {

    private final PrintStream output = System.out;
    private final BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    /**
     * Default constructor.<p>Calling the default constructor will initialise the class
     * and send the default announcement to the output device
     */
    public CLI() {
        hello();
    }

    private void hello() {
        output.println("\nErmintrude Command Line Interface\ninitialising, please wait...\n");
    }

    /**
     * Reads characters from the input device.<p>The read terminates if the character read is
     * a newline character (<code>'\n'</code>).
     *
     * @return a String containing characters read from the input device
     *         (excluding the terminating newline character).
     */
    public String read() {
        try {
            // Loop forever, reading the user's input
            while (true) {
                String prompt = "ermintrude> ";
                output.print(prompt);    // prompt the user
                output.flush();          // make the prompt appear immediately
                String buffer = input.readLine();  // get a line of input from the user

                if (buffer.length() == 0) continue;

                return buffer;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

}