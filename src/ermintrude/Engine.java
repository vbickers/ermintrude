package ermintrude;

import java.util.ArrayList;

import static ermintrude.Constants.*;

public final class Engine {

    static final int MAX_DEPTH_TO_SEARCH = 32;
    static final int CHECKMATE = 10000;

    static final int DRAW = 0;
    static final int ROOT = 0;
    static final int HORIZON = 0;

    static int examinedThisSearch = 0;

    static int maxPlyThisSearch = 0;

    static boolean checkmate = false;
    static boolean stalemate = false;
    static boolean startingInCheck = false;

    static MoveList candidates = new MoveList();
    static MoveList[] moveLists = new MoveList[MAX_DEPTH_TO_SEARCH];

    static int depthToSearch;

    static final int[] RAZORING_MARGIN = {0, VALUE_KNIGHT, VALUE_ROOK, VALUE_QUEEN};
    static final int[][] LMR_TABLE = new int[MAX_DEPTH_TO_SEARCH][MAX_DEPTH_TO_SEARCH];
    static void initialise() {

        for (int i = ROOT; i < MAX_DEPTH_TO_SEARCH; i++) {
            moveLists[i] = new MoveList();
            for (int j = ROOT; j < MAX_DEPTH_TO_SEARCH; j++) {
                LMR_TABLE[i][j] = 1 + (int) Math.ceil(Math.max(1, Math.log(i) * Math.log(j) / 2.0));
            }
        }

        checkmate = false;
        stalemate = false;
        startingInCheck = false;
    }

    static Move search() {

        int score;
        int ep_square = Board.EN_PASSANT_SQUARE;

        Move mv = new Move(NONE, 0);

        initialiseSearch();
        Clock.start();

        for (depthToSearch = 1; depthToSearch < MAX_DEPTH_TO_SEARCH; depthToSearch++) {

            if (isStalemate() || isCheckmate() || isDraw() || Clock.notEnoughTimeToContinue()) {
                break;
            }

            score = search(depthToSearch, -CHECKMATE, CHECKMATE, ROOT, false);
            if (!Clock.timeUp()) {
                updateBestMove(score, mv);
                displayMainLine(depthToSearch, score, examinedThisSearch, maxPlyThisSearch);
                if (Math.abs(score) > 9900) {
                    depthToSearch = MAX_DEPTH_TO_SEARCH;
                }
            }
        }

        Clock.stop();
        Board.EN_PASSANT_SQUARE = ep_square;

        return mv;
    }

    static boolean isStalemate() {
        return stalemate;
    }

    static void displayMainLine(int depth, int score, int nodes, int ply) {
        StringBuilder buf = new StringBuilder();
        buf.append(depth);
        while (buf.length() < 4) {
            buf.append(" ");
        }
        buf.append(score);
        while (buf.length() < 10) {
            buf.append(" ");
        }
        buf.append(Clock.elapsed() / 10);
        while (buf.length() < 15) {
            buf.append(" ");
        }
        buf.append(nodes);
        while (buf.length() < 27) {
            buf.append(" ");
        }
        buf.append(ply);
        while (buf.length() < 35) {
            buf.append(" ");
        }
        post(buf.toString());
        displayPrincipalVariation(Math.max(depth, maxPlyThisSearch));
        post("\n");
    }

    static void updateBestMove(int score, Move previousBest) {
        if (isStalemate() || isCheckmate() || isDraw()) {
            return;
        }
        Position p = OffHeapPositionHashTable.get(Board.HASH_VALUE);
        if (score == 9999 || score == -9999) {
            checkmate = true;
        }
        if (p != null && p.move != 0) {
            previousBest.move = p.move;
            previousBest.score = score;
        }
    }

    static boolean isCheckmate() {
        return checkmate;
    }

    static void initialiseSearch() {
        examinedThisSearch = NONE;
        maxPlyThisSearch = 0;
        detectGameOver();
        post(FENParser.getPosition() + "\n");
        OffHeapPositionHashTable.clear();
    }

    static void detectGameOver() {
        int checks = Board.countChecks();
        checkmate = stalemate = false;
        startingInCheck = (checks > 0);
        candidates.getMoves(checks, 1,0);
        if (candidates.size() == 0) {
            if (startingInCheck) {
                checkmate = true;
            }
            stalemate = true;
        }
    }

    static boolean isDraw() {
        return Board.insufficientMaterial() || Game.isDraw();
    }

    private static int search(int depth, int alpha, int beta, int ply, boolean allowNullMove) {

        if (Clock.timeUp() || ply >= MAX_DEPTH_TO_SEARCH) {
            return alpha;
        }

        //**********************************************************************
        // Handle repetitions - short circuit
        //**********************************************************************
        if (Game.isRepetition()) return -(DRAW - ply + 1);

        MoveList moves;

        int score = -CHECKMATE;
        int examinedThisPly = NONE;
        int bestMove = NONE;
        int hashMove = NONE;
        int checks = Board.countChecks();

        boolean inCheck = (checks > NONE);
        boolean pvNode = (alpha != beta - 1);
        boolean canPrune = !pvNode && Game.nothingToSeeHere();

        //**********************************************************************
        // Try to get a hashtable cutoff, otherwise set the hashed best move if
        // we've searched this position previously
        //**********************************************************************
        if (depth > HORIZON) {
            Position p = OffHeapPositionHashTable.get(Board.HASH_VALUE);
            if (p != null) {
                if (p.ply >= depth) {
                    if (p.flags == OffHeapPositionHashTable.EXACT) {
                        return p.score;
                    }
                    if (p.flags == OffHeapPositionHashTable.FAIL_HIGH) {
                        if (p.score >= beta) return p.score;
                        alpha = p.score;
                    } else if (p.flags == OffHeapPositionHashTable.FAIL_LOW) {
                        if (p.score <= alpha) return p.score;
                        beta = p.score;
                    }
                }
                hashMove = p.move;
            }
        }
        //**********************************************************************
        // Can we do a static evaluation and get a cut off? (horizon only)
        //**********************************************************************
        if (depth == HORIZON) {
            score = Evaluation.evaluate();

            if (score >= beta) return score;

            if (alpha < score) alpha = score;

        }
        //******************************************************************************
        // Q-SEARCH and MAIN-SEARCH: look for early exits. Only if we're not in check!
        //******************************************************************************
        if (!inCheck) {
            //**********************************************************************
            // Try delta pruning at the horizon
            //**********************************************************************
            if (depth == HORIZON) {
                int delta = Game.promoteThreat() ? 2 * VALUE_QUEEN - VALUE_PAWN : VALUE_QUEEN;
                if (score < alpha - delta) {
                    return score;
                }
            } else {
                //***********************************************************************
                // Not at the horizon. Can we try a null move and get a cut off?
                //***********************************************************************
                if (depth > 2 && allowNullMove) {
                    int ep_sq = Board.nullMove();
                    int r = Math.max(depth - 3, 3 + depth / 4);
                    int nullMoveScore = -search(depth - r, -beta, -beta + 1, ply + 1, false);
                    Board.takeBackNullMove(ep_sq);
                    if (nullMoveScore >= beta) {
                        return beta;
                    }
                }

                //***********************************************************************
                // Should we prune this branch entirely if it looks like a waste of time?
                //***********************************************************************
                if (canPrune && depth < RAZORING_MARGIN.length) {
                    int prune_value = RAZORING_MARGIN[depth];
                    int pruneScore = search(0, alpha - prune_value, alpha - prune_value + 1, ply, true);
                    if (pruneScore + prune_value <= alpha) {
                        return pruneScore;
                    }
                }
            }
        }

        //**********************************************************************
        // No early exits, so generate the moves and start searching...
        //**********************************************************************
        moves = moveLists[ply];
        moves.getMoves(checks, depth, hashMove);

        //***********************************************************************
        // Set the next search depth and ply and zero the current move
        // If we're at the horizon stay there.
        //***********************************************************************
        int nextDepth = depth == HORIZON ? depth : depth - 1;
        int nextPly = ply + 1;
        maxPlyThisSearch = nextPly;
        int mv = moves.pick(hashMove);

        //**********************************************************************
        // Play each move in turn and search the resulting position
        //**********************************************************************
        while (mv != NONE) {

            //*******************************************************************************
            // Make the move, and check its legal. Undo it and pick the next one if it isn't
            //*******************************************************************************
            if (!Board.move(mv)) {
                Board.takeBack(mv);
                mv = moves.pick();
                continue;
            }

            ++examinedThisSearch;
            ++examinedThisPly;

            //**********************************************************************
            // Do a full width search until we improve alpha, and then a zero
            // width search on the rest. The idea is that the first moves are most
            // likely best (due to move ordering), so the others should cut off quickly.
            // In the event they don't cut off as expected, research with full width.
            // For really boring looking moves, reduce the search depth (LMR)
            //**********************************************************************
            if (bestMove == NONE) {
                score = -search(nextDepth, -beta, -alpha, nextPly, true);
            } else {
//                if (canPrune && mv < MASK_CAPTURE && !inCheck) {
//                    int reduced_depth = Math.max(1, nextDepth - LMR_TABLE[Math.min(nextDepth, MAX_DEPTH_TO_SEARCH - 1)][Math.min(examinedThisPly, MAX_DEPTH_TO_SEARCH - 1)]);
//                    score = -search(reduced_depth - 1, -alpha - 1, -alpha, nextPly, true);
//                } else
                {
                    score = -search(nextDepth, -alpha - 1, -alpha, nextPly, true);
                }
                if (score > alpha && score < beta) { // zero window search did not cut off: move ordering problem, or positional surprise
                    score = -search(nextDepth, -beta, -score, nextPly, false);
                }
            }

            //**********************************************************************
            // Search of this move is over. undo it & find out how good/bad it was
            //**********************************************************************
            Board.takeBack(mv);

            //**********************************************************************
            // The move was so good, the opponent wouldn't have let us play it.
            // There is no point in continuing searching moves beyond this position.
            //**********************************************************************
            if (score >= beta) {
                if (depth > HORIZON) {
                    OffHeapPositionHashTable.put(Board.HASH_VALUE, depth, score, OffHeapPositionHashTable.FAIL_HIGH, mv);
                }
                return score;
            }

            //***********************************************************************
            // The move was better than anything so far. update score and remember it
            //***********************************************************************
            if (score > alpha) {

                alpha = score;
                bestMove = mv;

                //*******************************************************************
                // If the move was not a capture, record it as a killer move in this
                // position. This will help with move ordering when we get to look at
                // this position later (at deeper search iterations)
                //*******************************************************************
                if ((mv & MASK_CAPTURE) == NONE) {
                    Board.KILLER_MOVES[ply] = mv;
                }

            }

            mv = moves.pick();
        }

       //**********************************************************************
       // All legal moves have now been searched. Return the score and set
       // the position hash as appropriate.
       //**********************************************************************
        if (examinedThisPly == NONE && score == -CHECKMATE) {
            alpha = inCheck ? -(CHECKMATE - ply) : -ply + 1;
        }

        if (depth > HORIZON) {
            if (bestMove != NONE) {
                OffHeapPositionHashTable.put(Board.HASH_VALUE, depth, alpha, OffHeapPositionHashTable.EXACT, bestMove);
            } else {
                OffHeapPositionHashTable.put(Board.HASH_VALUE, depth, alpha, OffHeapPositionHashTable.FAIL_LOW, NONE);
            }
        }

        return alpha;

    }

    static void setTimeControl(int minutesForGame, int secondsIncrementPerMove) {
        Clock.setTimeControl(minutesForGame, secondsIncrementPerMove);
    }

    static void setTimeControl(int secondsPerMove) {
        Clock.setAverageMillisecondsPerMove(secondsPerMove, 0);
    }

    static void displayMove(int move, int piece, boolean moveGivesCheck) {
        post(Utils.shortFormat(move, piece, moveGivesCheck));
    }

    static void displayPrincipalVariation(int depth) {
        ArrayList<Move> moves = OffHeapPositionHashTable.mainLine(depth);
        int ply = Game.ply();
        for (Move m : moves) {
            int k = moves.indexOf(m);
            if (k > 0) {
                post(" ");
            }
            if ((k + ply) % 2 == 0) {
                post(((k + ply) / 2 + 1) + ". ");
            } else if (k == 0) {
                post(((k + ply) / 2 + 1) + " ..");
            }
            displayMove(m.move, m.piece, m.check);
        }
    }

    static void post(String s) {
        System.out.print(s);
    }

}