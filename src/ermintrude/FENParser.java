package ermintrude;

public final class FENParser {

    public static void setClassicalStartingPosition() {
        setPosition("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
    }
    public static void setPosition(String position) {

        Board.clear();

        char[] fen = position.toCharArray();
        char c;
        int i = 0;
        int x = 0;

        for (int rank = 0; rank < 8; rank++) {
            do {
                c = fen[i++];
                if ((int) c > 60) {
                    switch (c) {
                        case 'k':
                            Board.setPiece(x, Constants.BLACK_KING);
                            break;
                        case 'q':
                            Board.setPiece(x, Constants.BLACK_QUEEN);
                            break;
                        case 'r':
                            Board.setPiece(x, Constants.BLACK_ROOK);
                            break;
                        case 'b':
                            Board.setPiece(x, Constants.BLACK_BISHOP);
                            break;
                        case 'n':
                            Board.setPiece(x, Constants.BLACK_KNIGHT);
                            break;
                        case 'p':
                            Board.setPiece(x, Constants.BLACK_PAWN);
                            break;
                        case 'K':
                            Board.setPiece(x, Constants.WHITE_KING);
                            break;
                        case 'Q':
                            Board.setPiece(x, Constants.WHITE_QUEEN);
                            break;
                        case 'R':
                            Board.setPiece(x, Constants.WHITE_ROOK);
                            break;
                        case 'B':
                            Board.setPiece(x, Constants.WHITE_BISHOP);
                            break;
                        case 'N':
                            Board.setPiece(x, Constants.WHITE_KNIGHT);
                            break;
                        case 'P':
                            Board.setPiece(x, Constants.WHITE_PAWN);
                            break;
                    }
                    x++;
                } else
                    x += ((int) c - 48);

            } while (fen[i] != '/' && fen[i] != ' ');
            i++;
        }

        // Get the side to move
        c = fen[i];
        Board.SIDE_TO_MOVE = (c == 'W' || c == 'w') ? Constants.WHITE : Constants.BLACK;

        i += 2;

        // castling
        Board.CASTLED[Constants.WHITE] = 1;    // possibly wrong as player could have castled queen side!
        Board.MOVED[Constants.h1] = 1;
        Board.MOVED[Constants.a1] = 1;

        Board.CASTLED[Constants.BLACK] = 1;
        Board.MOVED[Constants.h8] = 1;
        Board.MOVED[Constants.a8] = 1;

        do {
            c = fen[i++];
            switch (c) {
                case 'K':
                    Board.CASTLED[Constants.WHITE] = 0;
                    Board.MOVED[Constants.h1] = 0;
                    break;
                case 'Q':
                    Board.CASTLED[Constants.WHITE] = 0;
                    Board.MOVED[Constants.a1] = 0;
                    break;
                case 'k':
                    Board.CASTLED[Constants.BLACK] = 0;
                    Board.MOVED[Constants.h8] = 0;
                    break;
                case 'q':
                    Board.CASTLED[Constants.BLACK] = 0;
                    Board.MOVED[Constants.a8] = 0;
                    break;
            }
        } while (fen[i] != ' ' && fen[i] != '-');
        i++;


        // ep-square
        if (fen[i] != '-') {
            String ep = position.substring(i, i + 2);
            Board.EN_PASSANT_SQUARE = Utils.getSquare(ep);
        } else {
            i += 2;
            int n = 0;
            StringBuilder hm = new StringBuilder();
            while (i < fen.length && fen[i] != ' ') {
                hm.append(fen[i++]);
            }
            if (hm.length() > 0) {
                n = Integer.parseInt(hm.toString());
            }
            // half-moves
            Game.reset(n);
        }
        // set the pieces map
        Board.BOARD_MAP = Board.WHITE_MAP | Board.BLACK_MAP;
        Board.rotate();

    }

    public static String getPosition() {

        StringBuilder buf = new StringBuilder();
        int empty = 0;

        for (int rank = 0; rank < 8; rank++) {
            if (rank > 0)
                buf.append("/");
            int rankOffset = rank << 3;
            for (int file = 0; file < 8; file++) {
                int piece = Board.SQUARE_HASH[rankOffset + file];
                if (piece != -1) {
                    if (empty > 0) {
                        buf.append(empty);
                        empty = 0;
                    }
                    buf.append(Piece.SYMBOL[piece]);
                } else
                    empty++;
            }
            if (empty > 0) {
                buf.append(empty);
                empty = 0;
            }
        }

        buf.append(Board.SIDE_TO_MOVE == Constants.WHITE ? " w" : " b");

        // TODO: castling rights, number of half-moves since last capture or pawn advance, full move number
        return buf.toString();
    }
}
