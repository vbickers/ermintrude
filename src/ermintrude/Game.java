package ermintrude;

import java.sql.Date;

import static ermintrude.Constants.*;

/**
 * This class records the game information. It is organised as an array of GameEntry instances
 * where each entry contains a move and the resulting position at a given gamePly. Additionally,
 * the GameTree maintains information about captures / pawn moves made by each side
 * permitting it to be used to quickly determine if a position is drawn by virtue of either
 * the three-fold repetition or the fifty-move rule.
 */
public final class Game {

    private static final int MAX_ENTRIES = 511;

    private static final GameEntry[] GE = new GameEntry[MAX_ENTRIES];
    private static final int[] breakPoint = new int[MAX_ENTRIES];
    private static final boolean[] promoteThreat = new boolean[MAX_ENTRIES];
    private static final boolean[] kingThreat = new boolean[MAX_ENTRIES];

    private static int bptPly = 0;
    private static int gamePly = 0;

    /**
     * Initialises the GameEntry array when the first reference to GameTree.instance is made.
     * Making this private ensures that no instances of the class can be created -
     * i.e. it's a true singleton. Making the class final ensures it is not inheritable from.
     */
    static void reset(int n) {
        for (int i = 0; i < MAX_ENTRIES; i++) {
            GE[i] = new GameEntry();
            GE[i].hash = i;
            breakPoint[i] = 0;
        }
        gamePly = n;
        bptPly = 0;
    }

    /**
     * Determines if the current position is a repetition of a previous one.
     */
    static boolean isRepetition() {
        long hash = GE[gamePly].hash;
        for (int j = gamePly - 4; j > breakPoint[bptPly]; j -= 4) {
            if (hash == GE[j].hash) {
                return true;
            }
        }
        return false;
    }

    static boolean isThreeFoldRepetition() {
        long hash = GE[gamePly].hash;
        int rep = 1;    // the current position has of course occurred once already... ;-)
        for (int j = gamePly - 4; j > breakPoint[bptPly]; j -= 4) {
            if (hash == GE[j].hash) {
                rep++;
                if (rep == 3) return true;
            }
        }
        return false;
    }

    static boolean promoteThreat() {
        return promoteThreat(gamePly);
    }

    /**
     * Determines if this position is a draw according to the fifty-move rule
     *
     * @return true if the current position is a draw by the fifty-move rule, false otherwise
     */
    static boolean isFiftyMoveDraw() {
        return ((gamePly - breakPoint[bptPly]) >= 100);
    }

    /**
     * Determines if this position is a true draw. Either the fifty-move rule will operate
     * or the last position in the game tree will have occurred twice before. This latter rule is
     * always derived from looking back up the game tree from the perspective of the last move played
     * and from the point of view of the player that made it. A position that recurs but with a different
     * player 'on the move' is not a repetition according to the laws of chess. To determine whether the
     * current position has repeated at least once before, use isRepetition(), not isDraw()
     *
     * @return true if the current position is a draw, false otherwise
     */
    static boolean isDraw() {
        return isThreeFoldRepetition() || isFiftyMoveDraw() || gamePly > 500;
    }

    /**
     * Add an entry to the game tree
     */
    static void update(int move, long hash, int piece, int ksw, int ksb) {
        gamePly++;

        GE[gamePly].move = move;
        GE[gamePly].hash = hash;

        // check for capture or pawn move and set a new draw test breakpoint if so
        promoteThreat[gamePly] = false;
        kingThreat[gamePly] = false;

        if (piece > WHITE_KING) {
            kingThreat[gamePly] = ((MoveVectors.KING_SAFETY_ZONE[ksw] & SQUARE_MASK[move & 0x3F]) != 0);
        } else {
            kingThreat[gamePly] = ((MoveVectors.KING_SAFETY_ZONE[ksb] & SQUARE_MASK[move & 0x3F]) != 0);
        }

        if (piece == WHITE_PAWN || piece == BLACK_PAWN || (move & MASK_CAPTURE) == MASK_CAPTURE) {
            bptPly++;
            breakPoint[bptPly] = gamePly;
            if (piece == WHITE_PAWN || piece == BLACK_PAWN) {
                int rank = RANK[move & 0x3F];
                promoteThreat[gamePly] = (rank == 1 || rank == 6);
            }
        }
    }


    /**
     * Clear an entry from the GameTree. Actually it does not do this at all, merely
     * decrements the game ply and any associated breakpoint.
     */
    static void undoLast() {
        gamePly--;
        if (breakPoint[bptPly] > gamePly) {
            bptPly--;
        }
    }

    /**
     * The GameEntry class records a move and the position hash resulting from the move.
     */
    private static class GameEntry {
        private int move = 0;
        private long hash = 0;
    }

    static boolean promoteThreat(int ply) {
        return promoteThreat[ply];
    }

    static boolean kingThreat() {
        return kingThreat[gamePly];
    }

    static boolean lastMoveQuiet() {
        return GE[gamePly].move < MASK_CAPTURE;
    }

    static boolean nothingToSeeHere() {
        return lastMoveQuiet() && !kingThreat() && !promoteThreat();
    }

    static void savePGN(String white, String black, int result, int tc1, int tc2) {
        StringBuffer buf = new StringBuffer();
        addPGNHeader(buf, "Date", new Date(System.currentTimeMillis()).toString());
        addPGNHeader(buf, "White", white);
        addPGNHeader(buf, "Black", black);
        addPGNHeader(buf, "Result", (result == 1) ? "1-0" : (result == -1) ? "0-1" : "1/2-1/2");
        addPGNHeader(buf, "TimeControl", (tc1 * 60) + "+" + tc2);
        for (int j = 1; j <= gamePly; j++) {
            addPGNMove(buf, j, GE[j].move);
        }
        System.out.println(buf);
    }

    private static void addPGNHeader(StringBuffer buf, String label, String value) {
        buf.append("[");
        buf.append(label);
        buf.append(" \"");
        buf.append(value);
        buf.append("\"]\n");
    }

    private static void addPGNMove(StringBuffer buf, int ply, int move) {
        if ((ply % 2) == 1) {
            buf.append((ply + 1) / 2);
            buf.append(". ");
        }
        buf.append(Utils.format(move));
        buf.append(" ");
    }

    static int ply() {
        return gamePly;
    }

    static int lastMove() {
        return GE[gamePly].move & 0xFFF;
    }

    public static void setOpening() {
    }

}