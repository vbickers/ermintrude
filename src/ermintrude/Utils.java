package ermintrude;

import static ermintrude.Constants.*;

import java.util.HashMap;

public final class Utils {

    private static final HashMap<String, Integer> squid = new HashMap<>();
    static  {
        squid.put("h1", 63);
        squid.put("g1", 62);
        squid.put("f1", 61);
        squid.put("e1", 60);
        squid.put("d1", 59);
        squid.put("c1", 58);
        squid.put("b1", 57);
        squid.put("a1", 56);
        squid.put("h2", 55);
        squid.put("g2", 54);
        squid.put("f2", 53);
        squid.put("e2", 52);
        squid.put("d2", 51);
        squid.put("c2", 50);
        squid.put("b2", 49);
        squid.put("a2", 48);
        squid.put("h3", 47);
        squid.put("g3", 46);
        squid.put("f3", 45);
        squid.put("e3", 44);
        squid.put("d3", 43);
        squid.put("c3", 42);
        squid.put("b3", 41);
        squid.put("a3", 40);
        squid.put("h4", 39);
        squid.put("g4", 38);
        squid.put("f4", 37);
        squid.put("e4", 36);
        squid.put("d4", 35);
        squid.put("c4", 34);
        squid.put("b4", 33);
        squid.put("a4", 32);
        squid.put("h5", 31);
        squid.put("g5", 30);
        squid.put("f5", 29);
        squid.put("e5", 28);
        squid.put("d5", 27);
        squid.put("c5", 26);
        squid.put("b5", 25);
        squid.put("a5", 24);
        squid.put("h6", 23);
        squid.put("g6", 22);
        squid.put("f6", 21);
        squid.put("e6", 20);
        squid.put("d6", 19);
        squid.put("c6", 18);
        squid.put("b6", 17);
        squid.put("a6", 16);
        squid.put("h7", 15);
        squid.put("g7", 14);
        squid.put("f7", 13);
        squid.put("e7", 12);
        squid.put("d7", 11);
        squid.put("c7", 10);
        squid.put("b7", 9);
        squid.put("a7", 8);
        squid.put("h8", 7);
        squid.put("g8", 6);
        squid.put("f8", 5);
        squid.put("e8", 4);
        squid.put("d8", 3);
        squid.put("c8", 2);
        squid.put("b8", 1);
        squid.put("a8", 0);
    }

    public static Integer getSquare(String sq) {
        return squid.get(sq);
    }

    public static String format(int move) {
        int from = (move >> 6) & 0x3F;
        int to = move & 0x3F;
        int flags = (move & MASK_FLAGS);
        String promote = "";
        if (flags > MASK_CAPTURE_EN_PASSANT) {
            if ((flags & MASK_PROMOTE_KNIGHT) == MASK_PROMOTE_KNIGHT) promote = "=n";
            if ((flags & MASK_PROMOTE_BISHOP) == MASK_PROMOTE_BISHOP) promote = "=b";
            if ((flags & MASK_PROMOTE_ROOK) == MASK_PROMOTE_ROOK) promote = "=r";
            if ((flags & MASK_PROMOTE_QUEEN) == MASK_PROMOTE_QUEEN) promote = "=q";
        }
        return (SQUARE_NAME[from] + SQUARE_NAME[to] + promote);
    }

    public static String shortFormat(int move, int piece, boolean moveGivesCheck) {

        String f = format(move);
        if (f.equals("e1c1") || f.equals("e8c8")) return "0-0-0";
        if (f.equals("e1g1") || f.equals("e8g8")) return "0-0";

        int flags = (move & MASK_FLAGS);
        int to = move & 0x3F;
        String capture = (flags & MASK_CAPTURE) == 0 ? "" : "x";
        String promote = "";
        String symbol = Piece.SHORT_FORMAT_SYMBOL[piece % 6];
        String check = moveGivesCheck ? "+" : "";
        return getString(move, piece, flags, to, capture, promote, symbol, check);
    }

    private static String getString(int move, int piece, int flags, int to, String capture, String promote, String symbol, String check) {
        if ((flags & MASK_CAPTURE) != 0 && (piece % 6) == 0) {
            symbol = SQUARE_NAME[(move >> 6) & 0x3F].substring(0, 1);
        }
        if (flags > MASK_CAPTURE_EN_PASSANT) {
            if ((flags & MASK_PROMOTE_KNIGHT) == MASK_PROMOTE_KNIGHT) promote = "=N";
            if ((flags & MASK_PROMOTE_BISHOP) == MASK_PROMOTE_BISHOP) promote = "=B";
            if ((flags & MASK_PROMOTE_ROOK) == MASK_PROMOTE_ROOK) promote = "=R";
            if ((flags & MASK_PROMOTE_QUEEN) == MASK_PROMOTE_QUEEN) promote = "=Q";
        }
        return symbol + capture + SQUARE_NAME[to] + promote + check;
    }

    public static String shortFormat(int move) {

        String f = format(move);
        if (f.equals("e1c1") || f.equals("e8c8")) return "0-0-0";
        if (f.equals("e1g1") || f.equals("e8g8")) return "0-0";

        int flags = (move & MASK_FLAGS);
        int from = (move >> 6) & 0x3F;
        int to = move & 0x3F;
        int piece = Board.SQUARE_HASH[from];
        String sep = (flags & MASK_CAPTURE) == 0 ? "" : "x";
        String promote = "";
        String symbol = Piece.SHORT_FORMAT_SYMBOL[piece % 6];
        Board.move(move);
        String check = Board.countChecks() > 0 ? "+" : "";
        Board.takeBack(move);
        return getString(move, piece, flags, to, sep, promote, symbol, check);
    }


    public static String format(int[] position) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < 64; i++) {
            int piece = position[i];
            if (i % 8 == 0) {
                s.append("\n");
            }
            if (piece > -1) {
                s.append(Piece.SYMBOL[piece]);
            } else {
                s.append(" ");
            }
            s.append(" ");
        }
        s.append("\n");
        return s.toString();
    }
}