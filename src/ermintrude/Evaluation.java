package ermintrude;

import static ermintrude.Constants.*;

public final class Evaluation {

    // bonuses for bishops (35)
    static final int BISHOP_PAIR_BONUS = 15;
    // bonuses for co-operating and dangerous rooks (35)
    static final int ROOK_ON_SEMI_OPEN_FILE_BONUS = 5;
    static final int ROOK_ON_OPEN_FILE_BONUS = 10;
    static final int ROOK_ON_SEVENTH_RANK_BONUS = 15;
    static final int ROOKS_ON_SAME_RANK_OR_FILE_BONUS = 20;

    // king safety (opening)
    static final int KING_NOT_CASTLED_PENALTY = -4;
    static final int KING_CANNOT_CASTLE_PENALTY = -10;
    static final int NO_KING_PAWN_SHIELD_PENALTY = -15;

    // pawn penalties and bonuses     // 5
    private static final int DOUBLED_PAWN_PENALTY = -5;
    private static final int BLOCKED_PAWN_PENALTY = -10;
    private static final int[] PASSED_PAWN_WHITE_BONUS = {0, 200, 100, 50, 25, 0, 0, 0};
    private static final int[] PASSED_PAWN_BLACK_BONUS = {0, 0, 0, 25, 50, 100, 200, 0};
    private static final int[] CONNECTED_PAWNS_WHITE_BONUS =
            {   0, 0, 0, 0, 0, 0, 0, 0,
                    30, 40, 50, 60, 60, 50, 40, 30,
                    20, 30, 40, 50, 50, 40, 30, 20,
                    10, 20, 30, 40, 40, 30, 20, 10,
                    0, 10, 20, 30, 30, 20, 10, 0,
                    0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0};
    private static final int[] CONNECTED_PAWNS_BLACK_BONUS =
            {   0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0,
                    0, 10, 20, 30, 30, 20, 10, 0,
                    10, 20, 30, 40, 40, 30, 20, 10,
                    20, 30, 40, 50, 50, 40, 30, 20,
                    30, 40, 50, 60, 60, 50, 40, 30,
                    0, 0, 0, 0, 0, 0, 0, 0};

    private static final PawnHashTable pawnHashTable = new PawnHashTable();

    private static final int[][] rooks = new int[2][2];

    static void echo(String msg) {
        System.out.println(msg);
    }

    static int evaluate() {

        int score = Board.MATERIAL_WHITE
                - Board.MATERIAL_BLACK
                + evalWhite() - evalBlack()
                + evalWhitePawns() - evalBlackPawns()
                + evalWhiteKing() - evalBlackKing() // king evaluation depends on other evaluations done first
                ;

        if (Board.SIDE_TO_MOVE == BLACK)
            score = -score;
        return score;
    }

    static int materialWhite() {
        return Board.MATERIAL_WHITE - 10000;
    }

    static int materialBlack() {
        return Board.MATERIAL_BLACK - 10000;
    }

    static int evalWhite() {
        int c = 0;
        int numberOfBishops = 0;
        int rookSquare = -1;
        rooks[0][0] = rookSquare;
        rooks[0][1] = rookSquare;
        long map = Board.WHITE_MAP ^ Board.WHITE_PAWNS;
        while (map != 0) {
            int sq = Bits.lsbValue(map);
            int p = Board.SQUARE_HASH[sq];
            if (p == WHITE_BISHOP) {
                numberOfBishops++;
                c += evalWhiteBishop(sq, numberOfBishops);
            } else if (p == WHITE_KNIGHT) {
                c += evalWhiteKnight(sq);
            } else if (p == WHITE_ROOK) {
                c += evalWhiteRook(sq, rookSquare);
                rookSquare = sq;
            } else if (p == WHITE_QUEEN) {
                c += evalWhiteQueen(sq);
            }
            map &= map - 1;
        }
        return c;
    }

    static int evalBlack() {
        int c = 0;
        int numberOfBishops = 0;
        int rookSquare = -1;
        rooks[1][0] = rookSquare;
        rooks[1][1] = rookSquare;
        long map = Board.BLACK_MAP ^ Board.BLACK_PAWNS;
        while (map != 0) {
            int sq = Bits.lsbValue(map);
            int p = Board.SQUARE_HASH[sq];
            if (p == BLACK_BISHOP) {
                ++numberOfBishops;
                c += evalBlackBishop(sq, numberOfBishops);
            } else if (p == BLACK_KNIGHT) {
                c += evalBlackKnight(sq);
            } else if (p == BLACK_ROOK) {
                c += evalBlackRook(sq, rookSquare);
                rookSquare = sq;
            } else if (p == BLACK_QUEEN) {
                c += evalBlackQueen(sq);
            }
            map &= map - 1;
        }
        return c;
    }

    static int evalWhiteKnight(int knightSquare) {
        // mobility
        return Bits.count(MoveGenerator.getKnightMoves(knightSquare, WHITE));
    }

    static int evalBlackKnight(int knightSquare) {
        // mobility
        return Bits.count(MoveGenerator.getKnightMoves(knightSquare, BLACK));
    }

    static int evalWhiteBishop(int bishopSquare, int numberOfBishops) {
        // mobility
        int c = Bits.count(MoveGenerator.getBishopMoves(bishopSquare, WHITE));
        if (numberOfBishops == 2) {
            c += BISHOP_PAIR_BONUS;
        }
        return c;// + BISHOP_POSITION_STATIC_SCORE[bishopSquare];
    }

    static int evalBlackBishop(int bishopSquare, int numberOfBishops) {
        // mobility
        int c = Bits.count(MoveGenerator.getBishopMoves(bishopSquare, BLACK));
        if (numberOfBishops == 2) {
            c += BISHOP_PAIR_BONUS;
        }
        return c;// + BISHOP_POSITION_STATIC_SCORE[bishopSquare];
    }

    static int evalWhiteRook(int rookSquare, int otherRookSq) {
        // general mobility
        int c = Bits.count(MoveGenerator.getRookMoves(rookSquare, WHITE));
        if (otherRookSq != -1) {
            rooks[0][1] = rookSquare;
            if (Constants.FILE[rookSquare] == Constants.FILE[otherRookSq]) {
                c += ROOKS_ON_SAME_RANK_OR_FILE_BONUS;
            } else if (Constants.RANK[rookSquare] == Constants.RANK[otherRookSq]) {
                c += ROOKS_ON_SAME_RANK_OR_FILE_BONUS;
            }
        } else {
            rooks[0][0] = rookSquare;
        }
        // rook on 7th
        if (Constants.RANK[rookSquare] == 1) {
            c += ROOK_ON_SEVENTH_RANK_BONUS;
        }
        // half-open/open files
        long fileMask = Constants.FILE_MASK_SQUARE[rookSquare];
        if ((fileMask & Board.BLACK_PAWNS) == 0) {
            c += ROOK_ON_SEMI_OPEN_FILE_BONUS;
            if ((fileMask & Board.WHITE_PAWNS) == 0) {
                c += ROOK_ON_OPEN_FILE_BONUS;
            }
        }
        return c;// + ROOK_POSITION_STATIC_SCORE[rookSquare];
    }

    static int evalBlackRook(int rookSquare, int otherRookSq) {
        // general mobility
        int c = Bits.count(MoveGenerator.getRookMoves(rookSquare, BLACK));
        // co-operating rooks
        if (otherRookSq != -1) {
            rooks[1][1] = rookSquare;
            if (Constants.FILE[rookSquare] == Constants.FILE[otherRookSq]) {
                c += ROOKS_ON_SAME_RANK_OR_FILE_BONUS;
            } else if (Constants.RANK[rookSquare] == Constants.RANK[otherRookSq]) {
                c += ROOKS_ON_SAME_RANK_OR_FILE_BONUS;
            }
        } else {
            rooks[1][0] = rookSquare;
        }
        // rook on 7th
        if (Constants.RANK[rookSquare] == 6) {
            c += ROOK_ON_SEVENTH_RANK_BONUS;
        }
        // half-open/open files
        long fileMask = Constants.FILE_MASK_SQUARE[rookSquare];
        if ((fileMask & Board.WHITE_PAWNS) == 0) {
            c += ROOK_ON_SEMI_OPEN_FILE_BONUS;
            if ((fileMask & Board.BLACK_PAWNS) == 0) {
                c += ROOK_ON_OPEN_FILE_BONUS;
            }
        }
        return c;// + ROOK_POSITION_STATIC_SCORE[rookSquare];
    }

    static int evalWhiteQueen(int queenSquare) {
        return Bits.count(MoveGenerator.getQueenMoves(queenSquare, WHITE)) + QUEEN_POSITION_STATIC_SCORE[queenSquare];
    }

    static int evalBlackQueen(int queenSquare) {
        return Bits.count(MoveGenerator.getQueenMoves(queenSquare, BLACK)) + QUEEN_POSITION_STATIC_SCORE[queenSquare];
    }

    static int evalWhiteKing() {
        int whitePieces = Bits.count(Board.WHITE_MAP ^ Board.WHITE_PAWNS);
        int c = 0;
        if (whitePieces < 4) {
//            c += Constants.KING_POSITION_STATIC_SCORE_END_GAME_PHASE[Board.KING_SQUARE_WHITE];
//            c += Bits.count(MoveGenerator.getKingMoves(WHITE));
        } else {
//            c += Constants.KING_POSITION_STATIC_SCORE_OPENING_PHASE[Board.KING_SQUARE_WHITE];
            if (Board.CASTLED[WHITE] != Board.NOT_CASTLED) {
                int file = FILE[Board.KING_SQUARE_WHITE];

                // penalize a hole in front of king
                long mask = FILE_MASK[file];
                if ((Board.WHITE_PAWNS & mask) == 0) {
                    c += NO_KING_PAWN_SHIELD_PENALTY;
                }

                // TODO? possible file attack

                // TODO? no pawns in front of king

                // TODO? penalize an open diagonal on the castled king

            } else {
                if (Board.MOVED[Board.KING_SQUARE_WHITE] > 0) {
                    c += KING_CANNOT_CASTLE_PENALTY;
                } else {
                    c += KING_NOT_CASTLED_PENALTY;
                    for (int i = 0; i < 2 && rooks[0][i] >= 0; i++) {
                        if (Board.MOVED[rooks[0][i]] > 0) {
                            c += KING_CANNOT_CASTLE_PENALTY;
                        }
                    }
                }
            }
        }
        return c;
    }

    static int evalBlackKing() {
        int black_pieces = Bits.count(Board.BLACK_MAP ^ Board.BLACK_PAWNS);
        int c = 0;
        if (black_pieces < 4) {
//            c += Constants.KING_POSITION_STATIC_SCORE_END_GAME_PHASE[Board.KING_SQUARE_BLACK];
//            c += Bits.count(MoveGenerator.getKingMoves(BLACK));
        } else {
//            c += Constants.KING_POSITION_STATIC_SCORE_OPENING_PHASE[63 - Board.KING_SQUARE_BLACK];
            if (Board.CASTLED[BLACK] != Board.NOT_CASTLED) {
                int file = FILE[Board.KING_SQUARE_BLACK];

                // no pawn in front of king
                long mask = FILE_MASK[file];
                if ((Board.BLACK_PAWNS & mask) == 0) {
                    c += NO_KING_PAWN_SHIELD_PENALTY;
                }

                // TODO? possible file attack

                // TODO? no pawns in front of king

                // TODO? penalize an open diagonal on the castled king

            } else {
                if (Board.MOVED[Board.KING_SQUARE_BLACK] > 0) {
                    c += KING_CANNOT_CASTLE_PENALTY;
                } else {
                    c += KING_NOT_CASTLED_PENALTY;
                    for (int i = 0; i < 2 && rooks[1][i] >= 0; i++) {
                        if (Board.MOVED[rooks[1][i]] > 0) {
                            c += KING_CANNOT_CASTLE_PENALTY;
                        }
                    }
                }
            }
        }
        return c;
    }

    static int evalWhitePawns() {

        PawnHashEntry entry = pawnHashTable.get(Board.WHITE_PAWNS);

        if (entry.key == Board.WHITE_PAWNS) {
            return entry.score;
        }

        int pawnScore = 0;

        long j = Board.WHITE_PAWNS;
        while (j != 0) {
            int sq = Bits.lsbValue(j);

            // doubled ?
            long fileMask = Constants.FILE_MASK_SQUARE[sq];
            long pawnOccupancyMask = 1L << sq;
            int file = Constants.FILE[sq];
            int rank = Constants.RANK[sq];
            if ((fileMask & Board.WHITE_PAWNS) != pawnOccupancyMask) {
                pawnScore += DOUBLED_PAWN_PENALTY;
            }

            // connected?
            long supporters = 0;
            if (file > 0) supporters |= Constants.FILE_MASK[file - 1] & Board.WHITE_PAWNS;
            if (file < 7) supporters |= Constants.FILE_MASK[file + 1] & Board.WHITE_PAWNS;
            if (supporters > 0) {
                pawnScore += CONNECTED_PAWNS_WHITE_BONUS[sq];
            }

            // passed?
            boolean passed = (fileMask & Board.BLACK_PAWNS) == 0;
            if (passed) {
                if (Board.BLACK_PAWNS == 0) {
                    pawnScore += PASSED_PAWN_WHITE_BONUS[Constants.RANK[sq]];
                } else {
                    if (file > 0) {
                        passed = (Constants.FILE_MASK[file - 1] & Board.BLACK_PAWNS) == 0;
                    }
                    if (passed && file < 7) {
                        passed = (Constants.FILE_MASK[file + 1] & Board.BLACK_PAWNS) == 0;
                    }
                    if (passed) {
                        pawnScore += PASSED_PAWN_WHITE_BONUS[rank];
                    }
                }
            }

            // TODO? unstoppable

            // blocked?
            if (Board.SQUARE_HASH[sq - 8] != -1) {
                pawnScore += BLOCKED_PAWN_PENALTY;
            }

            j &= j - 1;
        }
        entry.key = Board.WHITE_PAWNS;
        entry.score = pawnScore;
        return pawnScore;
    }

    static int evalBlackPawns() {

        PawnHashEntry entry = pawnHashTable.get(Board.BLACK_PAWNS);
        if (entry.key == Board.BLACK_PAWNS) {
            return entry.score;
        }

        int pawnScore = 0;
        long j = Board.BLACK_PAWNS;
        while (j != 0) {
            int sq = Bits.lsbValue(j);

            // doubled ?
            long fileMask = Constants.FILE_MASK_SQUARE[sq];
            long pawnOccupancyMask = 1L << sq;
            int file = Constants.FILE[sq];
            int rank = Constants.RANK[sq];
            if ((fileMask & Board.BLACK_PAWNS) != pawnOccupancyMask) {
                pawnScore += DOUBLED_PAWN_PENALTY;
            }

            // connected?
            long supporters = 0;
            if (file > 0) supporters |= Constants.FILE_MASK[file - 1] & Board.BLACK_PAWNS;
            if (file < 7) supporters |= Constants.FILE_MASK[file + 1] & Board.BLACK_PAWNS;
            if (supporters > 0) {
                pawnScore += CONNECTED_PAWNS_BLACK_BONUS[sq];
            }

            // passed?
            boolean passed = (fileMask & Board.WHITE_PAWNS) == 0;
            if (passed) {
                if (Board.WHITE_PAWNS == 0) {
                    pawnScore += PASSED_PAWN_BLACK_BONUS[Constants.RANK[sq]];
                } else {
                    if (file > 0) {
                        passed = (Constants.FILE_MASK[file - 1] & Board.WHITE_PAWNS) == 0;
                    }
                    if (passed && file < 7) {
                        passed = (Constants.FILE_MASK[file + 1] & Board.WHITE_PAWNS) == 0;
                    }
                    if (passed) {
                        pawnScore += PASSED_PAWN_BLACK_BONUS[rank];
                    }
                }
            }

            // TODO?: unstoppable

            // blocked?
            if (Board.SQUARE_HASH[sq + 8] != -1) {
                pawnScore += BLOCKED_PAWN_PENALTY;
            }

            // easy to forget!
            j &= j - 1;
        }

        entry.key = Board.BLACK_PAWNS;
        entry.score = pawnScore;
        return pawnScore;
    }

    public static void printEvaluation() {

        int w = 0, b = 0;

        echo(Utils.format(Board.SQUARE_HASH));
        echo("White pieces: " + Bits.count(Board.WHITE_MAP ^ Board.WHITE_PAWNS));
        echo("Black pieces: " + Bits.count(Board.BLACK_MAP ^ Board.BLACK_PAWNS));
        echo("----------------------------------------------------");
        w += Evaluation.materialWhite();
        w += Evaluation.evalWhite();
        w += Evaluation.evalWhitePawns();
        w += Evaluation.evalWhiteKing();
        echo("----------------------------------------------------");
        b += Evaluation.materialBlack();
        b += Evaluation.evalBlack();
        b += Evaluation.evalBlackPawns();
        b += Evaluation.evalBlackKing();
        echo("----------------------------------------------------");
        echo("White score: " + w);
        echo("Black score: " + b);

    }
}