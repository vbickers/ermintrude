package ermintrude;

public final class Move {

    public int move;
    public int score;
    public int piece;
    public boolean check;

    public Move() {
    }

    public Move(int move, int score) {
        this.move = move;
        this.score = score;
    }

    public String toString() {
        return Utils.format(move);
    }
}