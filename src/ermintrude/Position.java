package ermintrude;

public final class Position {

    long hash;    // 64 bits
    int move;     // 32 bits
    int score;    // 32 bits

    int ply;      // 32 bits
    int flags;    // 32 bits

}