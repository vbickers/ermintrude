package ermintrude;

public final class PawnHashTable {

    public int hashSize = 0;
    public int hashMask = 0;
    public int hashProbes = 0;
    public int hashHits = 0;
    public int hashCollisions = 0;

    private static PawnHashEntry[] ENTRY;

    public PawnHashTable() {
        ENTRY = new PawnHashEntry[8192];
        hashSize = 8192;
        hashMask = hashSize - 1; // mersenne prime
        for (int i = 0; i < 8192; i++)
            ENTRY[i] = new PawnHashEntry();
    }

    public PawnHashEntry get(long hash) {
        hashProbes++;
        PawnHashEntry e = ENTRY[(int) (hashMask & hash)];
        if (e.key == hash)
            hashHits++;
        else if (hash != 0)
            hashCollisions++;
        return e;
    }
}
