package ermintrude;

public class Configuration {

    private String position;
    private int secondsPerMove = 20; // default 20 seconds per move

    public Configuration(String[] args) {

        for (int i = 0; i < args.length; i += 2) {
            if (args[i].equals("-p")) position = args[i + 1];
            if (args[i].equals("-t")) secondsPerMove = Integer.parseInt(args[i + 1]);
            if (args[i].equals("-h")) {
                printHelpMessage();
                System.exit(0);
            }
        }
    }

    public String getPosition() {
        return position;
    }

    public int getSecondsPerMove() {
        return secondsPerMove;
    }

    private static void printHelpMessage() {
        System.out.println("usage:");
        System.out.println("java Ermintrude [<javaFlags>] [-t seconds per move] [-p \"fen\"]");
        System.out.println("\t:\"FEN\"= a chess position to start from, in Forsythe-Edwards Notation");
    }
}
