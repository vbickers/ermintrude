package ermintrude;/*
 * handles all opening book scenarios.
 */

import static ermintrude.Constants.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Objects;
import java.util.Random;
import java.util.StringTokenizer;

public final class Book {
    // some constants
    static final int NO_MOVE = 0;
    static final int FIRST_MOVE = 0;
    static final String BOOK = "ermintrude.book";

    static final int TOKEN_START = 0;
    static final int TOKEN_MVNUM = 1;
    static final int TOKEN_WMOVE = 2;
    static final int TOKEN_BMOVE = 3;

    // the book structure
    private HashMap<String, BookEntry> book = null;

    // our random number generator for choosing between moves
    private final Random random = new Random();

    // read in the opening book
    public void open() {
        BookEntry entry;
        ObjectInputStream bookFile = null;
        try {
            InputStream bk = this.getClass().getClassLoader().getResourceAsStream(BOOK);
            bookFile = new ObjectInputStream(bk);
            book = new HashMap<>();
            while ((entry = (BookEntry) bookFile.readObject()) != null) {
                book.put(entry.position, entry);
            }
            bookFile.close();
        } catch (java.io.EOFException eof) {
            try {
                if (bookFile != null) bookFile.close();
                System.out.println("read " + book.values().size() + " book lines");
            } catch (java.io.IOException ignored) {
            }
        } catch (Exception e) {
            System.out.println("Couldn't open book file. No openings book will be available");
        }
    }

    // returns a move from the book for the given position, if no move is
    // found, returns NO_MOVE
    public int pick(String position) {
        if (book != null) {
            BookEntry bookEntry = book.get(position);
            if (bookEntry != null) {
                if (bookEntry.position.equals(position)) {
                    int size = bookEntry.moves.size();
                    switch (size) {
                        case 0:
                            return NO_MOVE;
                        case 1:
                            return bookEntry.moves.get(FIRST_MOVE);
                        default:
                            return bookEntry.moves.get(random.nextInt(size));
                    }
                } else {
                    throw new RuntimeException("Book.pick(): book is corrupt");
                }
            }
        }
        return NO_MOVE;
    }

    public String opening(String position) {
        if (book != null) {
            BookEntry bookEntry = book.get(position);
            if (bookEntry != null) {
                if (bookEntry.position.equals(position)) {
                    return bookEntry.opening;
                }
            }
        }
        return "";
    }


    // creates the book from a text file.
    // the format of the text book must be as follows:
    // [optional]# comment\n (ignored)
    // name of opening[spaces]1.[spaces]move[spaces]{2.[spaces]move{[spaces]move|\n}|\n}
    // every move of every opening is stored to a positional hash. this is looked up
    // if the position is not found a new one is created, the name is stored and the move
    // if the position is found, the move is added to the list of moves from this position
    // which enables the program to handle transpositions
    public void create(String textFile) {
        int lineNumber = 0;

        BookEntry entry;

        String opening = "";
        StringBuilder variation = new StringBuilder();

        book = new HashMap<>();

        try {
            InputStream is = this.getClass().getClassLoader().getResourceAsStream(textFile);
            LineNumberReader reader = new LineNumberReader(new InputStreamReader(Objects.requireNonNull(is)));
            for (;;) {
                String line = reader.readLine();
                lineNumber = reader.getLineNumber();

                // end of book file
                if (line == null) break;

                // ignore comments
                if (line.charAt(0) != '#') {
                    Board.reset();

                    variation = new StringBuilder();

                    StringTokenizer tokenizer = new StringTokenizer(line);

                    int state = TOKEN_START;

                    while (tokenizer.hasMoreTokens()) {
                        String token = tokenizer.nextToken();

                        state++;

                        switch (state) {
                            case TOKEN_MVNUM:
                                break;
                            case TOKEN_WMOVE:
                            case TOKEN_BMOVE: {
                                // add the move
                                int move = parseMove(token, state);
                                entry = getEntry(FENParser.getPosition(), opening);
                                if (!entry.contains(move)) {
                                    entry.add(move);
                                }
                                // update the board position
                                Board.move(move);
                                variation.append(token).append(" ");
                                break;
                            }
                        }
                        if (state == TOKEN_BMOVE) state = TOKEN_START;
                    }
                } else {
                    opening = line.substring(2).trim();
                }
            }

            reader.close();
            save();
            open();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("\nBook.create() error at book line " + lineNumber + ": " + e.getMessage() + "\nOpening: " + opening + "\nVariation: " + variation);
        }
    }

    public int parseMove(String token, int state) {
        char[] move = token.toCharArray();
        switch (move[0]) {
            case 'N':
                return parseMove(KNIGHT, move);
            case 'B':
                return parseMove(BISHOP, move);
            case 'R':
                return parseMove(ROOK, move);
            case 'Q':
                return parseMove(QUEEN, move);
            case 'K':
                return parseMove(KING, move);
            case 'O':
                return parseCastleMove(move, state);
            case 'a':
            case 'b':
            case 'c':
            case 'd':
            case 'e':
            case 'f':
            case 'g':
            case 'h':
                return parseAmbiguousMove(move);
        }
        // if we get here, it is because we couldn't parse the move
        throw new RuntimeException("invalid move format: " + token);
    }

    private int parseMove(int pieceType, char[] move) {
        // generate list of valid moves at this position
        MoveList moves = new MoveList();
        moves.getMoves(Board.countChecks(), 1, 0);

        // find the last char of the actual move (check(mate) markers are removed)
        int last = move.length - 1;
        if (move[last] == '#') last--;
        else while (move[last] == '+') last--;

        // make sure there's no promotion information (TO DO) - not required in opening ;-)
        for (int i = 0; i < last; i++) {
            if (move[i] == '=') {
                last = i - 1;
                break;
            }
        }

        // get the text-formatted target square and convert to its square id
        String tgtSquare = String.valueOf(move[last - 1]) +
                move[last];

        int target = Utils.getSquare(tgtSquare);

        // check for piece ambiguity, i.e. more than one piece of designated type can
        // move to the target square. Such a move is always > 3 chars long, and the
        // second character is the rank or file the piece is moving from.
        // However, if the third char is an 'x' the move is not ambiguous - it is simply a capture.
        boolean unambiguous = !(last > 2 && move[1] != 'x');

        int disambiguatingPos = 1;    // disambiguate on file, 1,2,3,4,5...

        if (!unambiguous) {
            switch (move[1]) {
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'g':
                case 'h':
                    disambiguatingPos = 0;
            }
        }

        int m;
        while ((m = moves.pick()) != 0) {
            String mv = Utils.format(m);

            int fr = (m >> 6) & 0x3F;
            int to = m & 0x3F;

            if (to == target) {
                if ((Board.SQUARE_HASH[fr] % 6 + 1) == pieceType) {
                    if (pieceType != PAWN) {
                        if (unambiguous || mv.charAt(disambiguatingPos) == move[1]) return m;
                    } else {
                        if (mv.charAt(0) == move[0]) return m;
                    }
                }
            }
        }

        // if we get here, we couldn't validate the move
        throw new RuntimeException(Utils.format(Board.SQUARE_HASH) + "\nparseMove(): move " + new String(move) + " is invalid. valid moves are\n" + moves.simpleList());
    }

    private int parseCastleMove(char[] move, int state)  {
        // generate list of valid moves at this position
        MoveList moves = new MoveList();
        moves.getMoves(Board.countChecks(), 1,0);
        String mv;

        // determine the castle move we need
        if (move.length == 3) {
            if (state == TOKEN_WMOVE) mv = "e1g1";
            else mv = "e8g8";
        } else {
            if (state == TOKEN_WMOVE) mv = "e1c1";
            else mv = "e8c8";
        }

        // ensure its valid
        int m;
        while ((m = moves.pick()) != 0) {
            if (Utils.format(m).equals(mv)) return m;
        }

        // if we get here, we couldn't validate the move
        throw new RuntimeException(Utils.format(Board.SQUARE_HASH) + "\nparseCastleMove(): move '" + new String(move) + "' (" + mv + ") is invalid. valid moves are\n" + moves.simpleList());
    }

    // parse an ambiguous move. this is defined as a move starting with 'a' ... 'h'.
    // the move is ambiguous because its format is not yet known. it could be a short-form
    // move like c4, cxd5 or exf8=Q# in which case it is always a pawn move. Alternatively
    // it could be a long-format move of the form b1c3. If it is a short-form move, we delegate
    // to a parseMove(PAWN,...), otherwise we handle the move here.

    // NB. We must also handle abbreviated pawn capture syntax such as 'cd' In this case
    // the move string is only two chars long and contains only characters a - h.
    private int parseAmbiguousMove(char[] move) {
        MoveList moves = new MoveList();

        // check the first 2 chars, if they are a valid square and the length of the move
        // is 4 chars it must be a long format move, otherwise it is a short-form pawn move
        StringBuilder srcSquare = new StringBuilder();

        srcSquare.append(move[0]);
        srcSquare.append(move[1]);

        if (move.length == 4 && Utils.getSquare(srcSquare.toString()) != null) {
            // generate list of valid moves at this position
            moves.getMoves(Board.countChecks(), 1,0);

            String mv = new String(move);
            int m;
            while ((m = moves.pick()) != 0) {
                if (Utils.format(m).equals(mv)) return m;
            }
        } else {
            return parseMove(PAWN, move);
        }

        // if we get here, we couldn't validate the move
        throw new RuntimeException(Utils.format(Board.SQUARE_HASH) + "\nparseCastleMove(): move " + new String(move) + " is invalid. valid moves are\n" + moves.simpleList());
    }

    // saves the book
    public void save() {
        BookEntry entry;
        try {
            ObjectOutputStream bookFile = new ObjectOutputStream(Files.newOutputStream(Paths.get(BOOK)));
            for (BookEntry o : book.values()) {
                entry = o;
                bookFile.writeObject(entry);
            }
            bookFile.close();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Book.save() failed! " + e.getMessage());
        }
    }

    // return an entry from the book with the given position, or create a new one
    private BookEntry getEntry(String position, String opening) {
        BookEntry entry = book.get(position);
        if (entry == null) {
            entry = new BookEntry();
            entry.position = position;
            entry.opening = opening;
            book.put(position, entry);
        }
        return entry;
    }

    // recreate, save and then reload the opening book
    public static void main(String[] args) {
        Book book = new Book();
        book.create("eco");
    }
}