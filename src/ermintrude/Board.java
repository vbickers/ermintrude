package ermintrude;

import static ermintrude.Constants.*;

public final class Board {

    static final int NOT_CASTLED = 0;
    static final int CASTLED_KING_SIDE = 1;
    static final int CASTLED_QUEEN_SIDE = -1;

    static int EN_PASSANT_SQUARE = -1;

    static int[] MOVED = new int[64];

    public static int[][] CASTLE_MOVES = new int[3][2];
    static long[][] CASTLE_MASK = new long[3][2];
    static int[] CASTLED = {NOT_CASTLED, NOT_CASTLED, NOT_CASTLED};

    static int[] SQUARE_HASH = new int[64];

    static int[] KILLER_MOVES = new int[64];

    static long WHITE_MAP = 0L;
    static long BLACK_MAP = 0L;
    static long BOARD_MAP = 0L;

    static long WHITE_PAWNS = 0L;
    static long BLACK_PAWNS = 0L;
    static long PAWNS = 0L;

    static int SIDE_TO_MOVE = WHITE;
    static int OTHER_SIDE = BLACK;

    static int MATERIAL_WHITE = 0;
    static int MATERIAL_BLACK = 0;

    static int KING_SQUARE_WHITE = 0;
    static int KING_SQUARE_BLACK = 0;

    private static int[] CAPTURES_LIST = new int[30];
    private static int CAPTURES_INDEX = 0;

    // not should it should be here
    static long HASH_VALUE;


    // rotated boards. updated constantly as the position changes. used for bit-masking rook and bishop control vectors
    static long ROTATED_RIGHT_90 = 0L;
    static long ROTATED_RIGHT_45 = 0L;
    static long ROTATED_LEFT_45 = 0L;

    static long ATTACKER_MASK;
    static long CHECK_VECTOR;

    static void toggleSideToMove() {
        OTHER_SIDE = SIDE_TO_MOVE;
        SIDE_TO_MOVE ^= 3;
    }

    static void clear() {

        CASTLE_MOVES[WHITE][KING_SIDE] = 0x4F3E;    // e1g1
        CASTLE_MOVES[WHITE][QUEEN_SIDE] = 0x4F3A;   // e1c1
        CASTLE_MOVES[BLACK][KING_SIDE] = 0x4106;    // e8g8
        CASTLE_MOVES[BLACK][QUEEN_SIDE] = 0x4102;   // e8c8

        CASTLE_MASK[WHITE][KING_SIDE] = SQUARE_MASK[f1] | SQUARE_MASK[g1];
        CASTLE_MASK[WHITE][QUEEN_SIDE] = SQUARE_MASK[d1] | SQUARE_MASK[c1] | SQUARE_MASK[b1];
        CASTLE_MASK[BLACK][KING_SIDE] = SQUARE_MASK[f8] | SQUARE_MASK[g8];
        CASTLE_MASK[BLACK][QUEEN_SIDE] = SQUARE_MASK[d8] | SQUARE_MASK[c8] | SQUARE_MASK[b8];

        for (int i = 0; i < 64; i++) {
            SQUARE_HASH[i] = -1;
            MOVED[i] = 0;
        }

        MATERIAL_WHITE = 0;
        MATERIAL_BLACK = 0;

        BLACK_MAP = 0L;
        WHITE_MAP = 0L;
        BOARD_MAP = 0L;
        ROTATED_RIGHT_90 = 0L;
        ROTATED_RIGHT_45 = 0L;
        ROTATED_LEFT_45 = 0L;
        WHITE_PAWNS = 0L;
        BLACK_PAWNS = 0L;
        PAWNS = 0L;

        HASH_VALUE = 0;
        EN_PASSANT_SQUARE = -1;

        CASTLED[WHITE] = NOT_CASTLED;
        CASTLED[BLACK] = NOT_CASTLED;

        CAPTURES_LIST = new int[30];
        CAPTURES_INDEX = 0;

        Game.reset(0);
    }

    /*
     * Resets the board to the opening position. (i.e. a new game)
     */
    static void reset() {
        FENParser.setClassicalStartingPosition();
    }

    private static void doCastle(int fromSquare, int toSquare) {
        int rookFrom, rookTo, side;

        if (fromSquare == e1) {
            side = WHITE;
            if (toSquare == g1) {
                rookFrom = h1;
                rookTo = f1;
                CASTLED[WHITE] = CASTLED_KING_SIDE;
            } else {
                rookFrom = a1;
                rookTo = d1;
                CASTLED[WHITE] = CASTLED_QUEEN_SIDE;
            }
        } else {
            side = BLACK;
            if (toSquare == g8) {
                rookFrom = h8;
                rookTo = f8;
                CASTLED[BLACK] = CASTLED_KING_SIDE;
            } else {
                rookFrom = a8;
                rookTo = d8;
                CASTLED[BLACK] = CASTLED_QUEEN_SIDE;
            }
        }
        int piece = SQUARE_HASH[rookFrom];

        SQUARE_HASH[rookTo] = SQUARE_HASH[rookFrom];
        SQUARE_HASH[rookFrom] = -1;

        // ensure the board bitmaps are updated to reflect the changed rook
        // position, as well as the new positional score
        if (side == WHITE) {
            WHITE_MAP ^= (SQUARE_MASK[rookFrom] | SQUARE_MASK[rookTo]);
        } else {
            BLACK_MAP ^= (SQUARE_MASK[rookFrom] | SQUARE_MASK[rookTo]);
        }
        BOARD_MAP = WHITE_MAP | BLACK_MAP;
        updateRotations(rookFrom, rookTo);

        HASH_VALUE ^= (ZobristHash.hash[piece][rookFrom] | ZobristHash.hash[piece][rookTo]);

    }

    private static void undoCastle(int fromSquare, int toSquare) {
        int rookFrom, rookTo, side;

        if (fromSquare == e1) {
            side = WHITE;
            if (toSquare == g1) {
                rookFrom = h1;
                rookTo = f1;
            } else {
                rookFrom = a1;
                rookTo = d1;
            }
        } else {
            side = BLACK;
            if (toSquare == g8) {
                rookFrom = h8;
                rookTo = f8;
            } else {
                rookFrom = a8;
                rookTo = d8;
            }
        }
        int piece = SQUARE_HASH[rookTo];
        SQUARE_HASH[rookFrom] = SQUARE_HASH[rookTo];
        SQUARE_HASH[rookTo] = -1;

        // ensure the board bitmaps are updated to reflect the changed rook
        // position as well as the positional scores
        if (side == WHITE) {
            WHITE_MAP ^= (SQUARE_MASK[rookFrom] | SQUARE_MASK[rookTo]);
        } else {
            BLACK_MAP ^= (SQUARE_MASK[rookFrom] | SQUARE_MASK[rookTo]);
        }
        BOARD_MAP = WHITE_MAP | BLACK_MAP;

        updateRotations(rookFrom, rookTo);
        HASH_VALUE ^= (ZobristHash.hash[piece][rookFrom] | ZobristHash.hash[piece][rookTo]);

        CASTLED[side] = NOT_CASTLED;
    }

    private static void updateRotations(int fromSquare, int toSquare) {
        ROTATED_RIGHT_90 ^= (SQUARE_MASK_R90[fromSquare] | SQUARE_MASK_R90[toSquare]);
        ROTATED_RIGHT_45 ^= (SQUARE_MASK_R45[fromSquare] | SQUARE_MASK_R45[toSquare]);
        ROTATED_LEFT_45 ^= (SQUARE_MASK_L45[fromSquare] | SQUARE_MASK_L45[toSquare]);
    }

    private static void xorRotation(int square) {
        ROTATED_RIGHT_90 ^= (SQUARE_MASK_R90[square]);
        ROTATED_RIGHT_45 ^= (SQUARE_MASK_R45[square]);
        ROTATED_LEFT_45 ^= (SQUARE_MASK_L45[square]);
    }

    static int nullMove() {
        toggleSideToMove();
        HASH_VALUE ^= -1L;
        int ep = EN_PASSANT_SQUARE;
        EN_PASSANT_SQUARE = -1;
        return ep;
    }

    static void takeBackNullMove(int ep) {
        toggleSideToMove();
        HASH_VALUE ^= -1L;
        EN_PASSANT_SQUARE = ep;
    }

    // make a move (move a piece)
    // the move consists of to and from squares in the lower 12 bits.
    // if a move is a non-ep capture, the 13th bit is set
    // if the move is a castle or e.p. capture the 14th bit is set
    // if the promotion bit is on, bits 15 & 16 are set: 00 = n, 01=b 10 = r 11=q
    // bit 14 therefore does triple duty, and context from the 12 bits previous is required to determine what it means
    // returns true if the move was legal
    static boolean move(int move) {

        int toSquare = (move & 0x3F);
        int fromSquare = (move >> 6) & 0x3F;

        long updateMask = SQUARE_MASK[toSquare] | SQUARE_MASK[fromSquare];

        int fPiece = SQUARE_HASH[fromSquare];
        int tPiece = fPiece;

        EN_PASSANT_SQUARE = -1;
        int flags = move & MASK_FLAGS;

        if ((flags & MASK_CAPTURE) == MASK_CAPTURE) {
            capture(toSquare);
        } else if ((flags ^ MASK_CASTLE) == 0) {
            doCastle(fromSquare, toSquare);
        } else if ((flags ^ MASK_CAPTURE_EN_PASSANT) == 0) {
            capture(SIDE_TO_MOVE == WHITE ? toSquare + 8 : toSquare - 8);
        }
        if (flags > MASK_CAPTURE_EN_PASSANT) {
            if (SIDE_TO_MOVE == WHITE) {
                if ((flags & MASK_PROMOTE_QUEEN) == MASK_PROMOTE_QUEEN) tPiece = WHITE_QUEEN;
                else if ((flags & MASK_PROMOTE_ROOK) == MASK_PROMOTE_ROOK) tPiece = WHITE_ROOK;
                else if ((flags & MASK_PROMOTE_BISHOP) == MASK_PROMOTE_BISHOP) tPiece = WHITE_BISHOP;
                else if ((flags & MASK_PROMOTE_KNIGHT) == MASK_PROMOTE_KNIGHT) tPiece = WHITE_KNIGHT;
                WHITE_PAWNS ^= SQUARE_MASK[fromSquare];
                MATERIAL_WHITE += (Piece.VALUE[tPiece] - VALUE_PAWN);
            } else {
                if ((flags & MASK_PROMOTE_QUEEN) == MASK_PROMOTE_QUEEN) tPiece = BLACK_QUEEN;
                else if ((flags & MASK_PROMOTE_ROOK) == MASK_PROMOTE_ROOK) tPiece = BLACK_ROOK;
                else if ((flags & MASK_PROMOTE_BISHOP) == MASK_PROMOTE_BISHOP) tPiece = BLACK_BISHOP;
                else if ((flags & MASK_PROMOTE_KNIGHT) == MASK_PROMOTE_KNIGHT) tPiece = BLACK_KNIGHT;
                BLACK_PAWNS ^= SQUARE_MASK[fromSquare];
                MATERIAL_BLACK += (Piece.VALUE[tPiece] - VALUE_PAWN);
            }
        }
        // set the ep-square if we've moved a pawn two squares
        if ((fPiece == WHITE_PAWN || fPiece == BLACK_PAWN) && DISTANCE[fromSquare][toSquare] == 2) {
            EN_PASSANT_SQUARE = (fromSquare + toSquare) >> 1;
        }

        // move the piece to the target square, and remove it from its previous location
        SQUARE_HASH[toSquare] = tPiece;
        SQUARE_HASH[fromSquare] = -1;

        // log the move
        MOVED[fromSquare]++;

        //  ly update bitmaps (included rotated ones), the hash value for the position and positional scores.
        HASH_VALUE ^= (ZobristHash.hash[fPiece][fromSquare] | ZobristHash.hash[tPiece][toSquare]);
        if (SIDE_TO_MOVE == WHITE) {
            WHITE_MAP ^= updateMask;
            if (fPiece == tPiece) { // not a promotion move
                if (fPiece == WHITE_PAWN) {
                    WHITE_PAWNS ^= updateMask;
                } else if (fPiece == WHITE_KING) {
                    KING_SQUARE_WHITE = toSquare;
                }
            }
        } else {
            HASH_VALUE ^= ZobristHash.hash_black;
            BLACK_MAP ^= updateMask;
            if (fPiece == tPiece) {// not a promotion
                if (fPiece == BLACK_PAWN) {
                    BLACK_PAWNS ^= updateMask;
                } else if (fPiece == BLACK_KING) {
                    KING_SQUARE_BLACK = toSquare;
                }
            }
        }

        BOARD_MAP = WHITE_MAP | BLACK_MAP;
        PAWNS = WHITE_PAWNS | BLACK_PAWNS;

        updateRotations(fromSquare, toSquare);

        Game.update(move, HASH_VALUE, fPiece, KING_SQUARE_WHITE, KING_SQUARE_BLACK);

        boolean legalMove = (!MoveGenerator.isKingAttacked());
        toggleSideToMove();

        return legalMove;
    }

    /*
     * undo a move made previously. the steps are the reverse of making the move
     */
    static void takeBack(int move) {
        toggleSideToMove();

        int toSquare = move & 0x3F;
        int fromSquare = (move >> 6) & 0x3F;

        long updateMask = (1L << toSquare) | (1L << fromSquare);

        int tPiece = SQUARE_HASH[toSquare];
        int fPiece = tPiece;

        int flags = move & MASK_FLAGS;

        // check whether we are undoing a promotion move. update material
        // value and pawn hash values
        // should really also update pawn position values, because we now
        // have a pawn about to promote
        if (flags > MASK_CAPTURE_EN_PASSANT) {
            if (SIDE_TO_MOVE == WHITE) {
                fPiece = WHITE_PAWN;
                WHITE_PAWNS ^= (1L << fromSquare);
                MATERIAL_WHITE -= (Piece.VALUE[tPiece] - Constants.VALUE_PAWN);
            } else {
                fPiece = BLACK_PAWN;
                BLACK_PAWNS ^= (1L << fromSquare);
                MATERIAL_BLACK -= (Piece.VALUE[tPiece] - Constants.VALUE_PAWN);
            }
        }

        // move the piece back
        SQUARE_HASH[fromSquare] = fPiece;
        SQUARE_HASH[toSquare] = -1;

        // was it a non-e.p. capture? if so, un-capture enemy piece first
        EN_PASSANT_SQUARE = -1;
        if ((flags & MASK_CAPTURE) == MASK_CAPTURE) {
            unCapture(toSquare);
        } else if ((flags ^ MASK_CASTLE) == 0) {
            undoCastle(fromSquare, toSquare);
        } else if ((flags ^ MASK_CAPTURE_EN_PASSANT) == 0) {
            unCapture(SIDE_TO_MOVE == WHITE ? toSquare + 8 : toSquare - 8);
            EN_PASSANT_SQUARE = toSquare;
        }

        // un-log the move
        MOVED[fromSquare]--;

        // update the hash
        HASH_VALUE ^= (ZobristHash.hash[fPiece][fromSquare] | ZobristHash.hash[tPiece][toSquare]);
        if (SIDE_TO_MOVE == WHITE) {
            WHITE_MAP ^= updateMask;
            if (tPiece == fPiece) {
                if (tPiece == WHITE_PAWN) {
                    WHITE_PAWNS ^= updateMask;
                } else if (tPiece == WHITE_KING) {
                    KING_SQUARE_WHITE = fromSquare;
                }
            }
        } else {
            HASH_VALUE ^= ZobristHash.hash_black;
            BLACK_MAP ^= updateMask;
            if (tPiece == fPiece) {
                if (tPiece == BLACK_PAWN) {
                    BLACK_PAWNS ^= updateMask;
                } else if (tPiece == BLACK_KING) {
                    KING_SQUARE_BLACK = fromSquare;
                }
            }
        }

        BOARD_MAP = WHITE_MAP | BLACK_MAP;
        PAWNS = WHITE_PAWNS | BLACK_PAWNS;
        updateRotations(toSquare, fromSquare);

        Game.undoLast();

    }

    // add used only during setup of a position
    static void setPiece(int square, int piece) {
        if (piece != -1) {
            SQUARE_HASH[square] = piece;
            MOVED[square] = (Piece.isHomeSquare(piece, square)) ? 0 : 1;
            if (piece < BLACK_PAWN) {
                MATERIAL_WHITE += Piece.VALUE[piece];
                WHITE_MAP |= (1L << square);
                switch ((piece % 6 + 1)) {
                    case PAWN:
                        WHITE_PAWNS |= (1L << square);
                        break;
                    case KING:
                        KING_SQUARE_WHITE = square;
                        break;
                }
            } else {
                BLACK_MAP |= (1L << square);
                MATERIAL_BLACK += Piece.VALUE[piece];
                switch ((piece % 6 + 1)) {
                    case PAWN:
                        BLACK_PAWNS |= (1L << square);
                        break;
                    case KING:
                        KING_SQUARE_BLACK = square;
                        break;
                }
            }
            BOARD_MAP = WHITE_MAP | BLACK_MAP;
            PAWNS = WHITE_PAWNS | BLACK_PAWNS;
            HASH_VALUE ^= ZobristHash.hash[piece][square];
        } else {
            throw new RuntimeException("setPiece(): piece is null or empty");
        }
    }

    private static void capture(int square) {

        int piece = SQUARE_HASH[square];

        CAPTURES_LIST[CAPTURES_INDEX++] = piece; // save the victim in the capture list
        SQUARE_HASH[square] = -1;                 // clear the victim's square
        MOVED[square]++;                     //
        HASH_VALUE ^= ZobristHash.hash[piece][square];

        // update the values for the victim
        if (piece < BLACK_PAWN) {
            WHITE_MAP ^= (1L << square);
            MATERIAL_WHITE -= Piece.VALUE[piece];
            if (piece == WHITE_PAWN) {
                WHITE_PAWNS ^= (1L << square);
            }
        } else {
            BLACK_MAP ^= (1L << square);
            MATERIAL_BLACK -= Piece.VALUE[piece];
            if (piece == BLACK_PAWN) {
                BLACK_PAWNS ^= (1L << square);
            }
        }
        xorRotation(square);
    }

    private static void unCapture(int square) {

        int piece = CAPTURES_LIST[--CAPTURES_INDEX];

        SQUARE_HASH[square] = piece;
        MOVED[square]--;
        HASH_VALUE ^= ZobristHash.hash[piece][square];

        if (piece < BLACK_PAWN) {
            WHITE_MAP |= (1L << square);
            MATERIAL_WHITE += Piece.VALUE[piece];
            if (piece == WHITE_PAWN) {
                WHITE_PAWNS |= (1L << square);
            }
        } else {
            BLACK_MAP |= (1L << square);
            MATERIAL_BLACK += Piece.VALUE[piece];
            if (piece == BLACK_PAWN) {
                BLACK_PAWNS |= (1L << square);
            }
        }
        xorRotation(square);
    }


    static void rotate() {
        ROTATED_RIGHT_90 = 0;
        ROTATED_RIGHT_45 = 0;
        ROTATED_LEFT_45 = 0;
        for (int sq = 0; sq < 64; sq++) {
            long p = (1L << sq);
            if ((BOARD_MAP & p) != 0) {
                ROTATED_RIGHT_90 |= SQUARE_MASK_R90[sq];
                ROTATED_RIGHT_45 |= SQUARE_MASK_R45[sq];
                ROTATED_LEFT_45 |= SQUARE_MASK_L45[sq];
            }
        }
    }

    static long getAttacksTo(int sq) {

        int offset = sq << 8;

        int occupancyMaskRank = (int) (BOARD_MAP >> (RANK[sq] << 3L)) & 0xFF;
        int occupancyMaskFile = (int) (ROTATED_RIGHT_90 >> (FILE[sq] << 3L)) & 0xFF;
        long moves = MoveVectors.ROOK_MOVE_VECTORS_RANK_64[offset | occupancyMaskRank];

        moves |= MoveVectors.ROOK_MOVE_VECTORS_FILE_64[offset | occupancyMaskFile];

        int occupancyMask = (int) ((ROTATED_RIGHT_45 >> VECTOR_SHIFTS_A1_H8[sq]) & VECTOR_MASK_A1_H8[sq]);
        moves |= MoveVectors.BISHOP_MOVE_VECTORS_A1_H8_64[offset | occupancyMask];

        occupancyMask = (int) ((ROTATED_LEFT_45 >> VECTOR_SHIFTS_A8_H1[sq]) & VECTOR_MASK_A8_H1[sq]);
        moves |= MoveVectors.BISHOP_MOVE_VECTORS_A8_H1_64[offset | occupancyMask];

        moves |= MoveVectors.KNIGHT_MOVE_VECTORS[sq];

        return moves;
    }

    static int countAttacks(int sq) {
        int count = 0;
        CHECK_VECTOR = 0;
        long possibleAttacks = getAttacksTo(sq);
        if (SIDE_TO_MOVE == WHITE) {
            possibleAttacks &= BLACK_MAP;
        } else {
            possibleAttacks &= WHITE_MAP;
        }
        while (possibleAttacks != 0) {
            int t = Bits.lsbValue(possibleAttacks);
            if (MoveGenerator.isLegalAttack(t, sq)) {
                count++;
                ATTACKER_MASK = SQUARE_MASK[t];
                CHECK_VECTOR |= MoveGenerator.getAttackVector(t, sq);
            }
            possibleAttacks &= possibleAttacks -1;
        }
        return count;
    }


    static int countChecks() {
        if (SIDE_TO_MOVE == WHITE)
            return countAttacks(KING_SQUARE_WHITE);
        else
            return countAttacks(KING_SQUARE_BLACK);
    }

    static boolean insufficientMaterial() {
        return PAWNS == 0 && MATERIAL_WHITE < VALUE_KING + VALUE_ROOK && MATERIAL_BLACK < VALUE_KING + VALUE_ROOK;
    }

    public static long playerMap() {
        return SIDE_TO_MOVE == WHITE ? WHITE_MAP : BLACK_MAP;
    }
}