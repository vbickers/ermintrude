package ermintrude;/*
 * defines a single book entry
 */

import java.io.Serializable;
import java.util.Vector;

public final class BookEntry implements Serializable {
    private static final long serialVersionUID = 7345651181383183265L;

    String position = "";
    Vector<Integer> moves = new Vector<>();
    String opening = "";

    public boolean contains(int move) {
        Integer mv = move;
        return moves.contains(mv);
    }

    public void add(int move) {
        if (!moves.add(move)) {
            throw new RuntimeException("BookEntry.add() failed!");
        }
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(opening);
        s.append(" ");
        s.append(position);
        for (Integer move : moves) {
            s.append(" ");
            s.append(Utils.format(move));
        }
        return s.toString();
    }
}