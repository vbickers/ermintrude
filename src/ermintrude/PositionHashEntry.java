package ermintrude;

/**
 * HashEntry
 */
public class PositionHashEntry {
    static final int SIZE = 3;
    static final int SIZE_IN_BYTES = SIZE * 8;
    static final int HASH_OFFSET = 0;      // hash
    static final int HASH_OFFSET_IN_BYTES = 0;      // hash
    static final int SCORE_OFFSET = 1;     // move (32 bits) + score (32 bits)
    static final int SCORE_OFFSET_IN_BYTES = 8;     // move (32 bits) + score (32 bits)
    static final int PLY_OFFSET = 2;       // ply (32 bits) + flags (32 bits)
    static final int PLY_OFFSET_IN_BYTES = 16;       // ply (32 bits) + flags (32 bits)
}
