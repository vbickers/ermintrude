package ermintrude;

public final class Clock {

    private static long elapsed = 0L;
    private static long started = 0L;

    private static long timeRemainingMilliseconds = 0;

    private static int movesRemaining = 40;

    private static int averageMillisecondsPerMove = 20000;

    static void setTimeControl(int minutesForEntireGame, int secondsIncrementPerMove) {
        // TODO: improve this to handle number of moves and add increment where it is non-zero
        Clock.timeRemainingMilliseconds = minutesForEntireGame * 60000L; //seconds
        Clock.movesRemaining = 40;
        // tries to set an average time for each move
        setAverageMillisecondsPerMove(minutesForEntireGame * 60 / Clock.movesRemaining, secondsIncrementPerMove);
    }

    static void setAverageMillisecondsPerMove(int secondsPerMove, int secondsIncrementPerMove) {
        averageMillisecondsPerMove = secondsPerMove * 1000 + secondsIncrementPerMove * Clock.movesRemaining;
    }

    static boolean notEnoughTimeToContinue() {
        long now = System.currentTimeMillis();      // eg. 58000
        long elapsed = now - started;               // eg  58000 - 57000 = 1000    = ELAPSED
        return elapsed * 2 >= averageMillisecondsPerMove;
    }

    static boolean timeUp() {
        long now = System.currentTimeMillis();      // eg. 58000
        long elapsed = now - started;               // eg  58000 - 57000 = 1000    = ELAPSED
        return elapsed >= averageMillisecondsPerMove;
    }

    static void start() {
        started = System.currentTimeMillis();
        elapsed = -started;
    }

    static long elapsed() {
        return System.currentTimeMillis() - started;
    }

    static void stop() {
        long now = System.currentTimeMillis();
        elapsed += now;
        timeRemainingMilliseconds = timeRemainingMilliseconds - elapsed;
    }

}