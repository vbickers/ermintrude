package ermintrude;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;

public final class ZobristHash {

    public static long[][] hash = new long[12][64];
    public static long hash_white;
    public static long hash_black;

    private static final String ZOB_KEY_FILE = "hkzob_long";

    static {
        if (!loadKeys()) {
            createKeys();
            dumpKeys();
        }
    }

    private static void createKeys() {
        System.out.println("Info: creating new hash keys");
        Random rand = new Random(System.currentTimeMillis());
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 64; j++) {
                hash[i][j] = rand.nextLong();
            }
        }
        hash_white = rand.nextLong();
        hash_black = rand.nextLong();
    }

    // dumps the current set of zob keys
    public static void dumpKeys() {
        try {
            ObjectOutputStream hashKeys = new ObjectOutputStream(Files.newOutputStream(Paths.get(ZOB_KEY_FILE)));
            for (int i = 0; i < 12; i++) {
                for (int j = 0; j < 64; j++) {
                    hashKeys.writeLong(hash[i][j]);
                }
            }
            hashKeys.writeLong(hash_white);
            hashKeys.writeLong(hash_black);
            hashKeys.close();
        } catch (Exception e) {
            System.out.println("Warning: couldn't save hash keys");
        }
    }

    // loads a set of zob keys from an external file
    public static boolean loadKeys() {
        try {
            ObjectInputStream hashKeys = new ObjectInputStream(Files.newInputStream(Paths.get(ZOB_KEY_FILE)));
            for (int i = 0; i < 12; i++) {
                for (int j = 0; j < 64; j++) {
                    hash[i][j] = hashKeys.readLong();
                }
            }
            hash_white = hashKeys.readLong();
            hash_black = hashKeys.readLong();
            hashKeys.close();
            return true;
        } catch (Exception e) {
            System.out.println("Warning: couldn't load hash keys");
            return false;
        }
    }

    public static void main(String[] args) {
        loadKeys();
    }
}