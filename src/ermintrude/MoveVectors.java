package ermintrude;

import static ermintrude.Constants.*;

public final class MoveVectors {

    static final long[] KNIGHT_MOVE_VECTORS = new long[64];
    static final long[] KING_MOVE_VECTORS = new long[64];

    static final long[] BISHOP_MOVE_VECTORS_A1_H8_64 = new long[16384];
    static final long[] BISHOP_MOVE_VECTORS_A8_H1_64 = new long[16384];
    static final long[] ROOK_MOVE_VECTORS_RANK_64 = new long[16384];
    static final long[] ROOK_MOVE_VECTORS_FILE_64 = new long[16384];

    static final int[] ATTACKER_64 = new int[4096];
    static final long[] ROOK_DEFENSE_VECTORS_64 = new long[4096];
    static final long[] BISHOP_DEFENSE_VECTORS_64 = new long[4096];

    static long[] KING_EXCLUSION_ZONE;
    static boolean[][][] PAWN_ATTACK;
    static long[] KING_SAFETY_ZONE;

    // generates the move vectors for all pieces on all squares on an empty board
    static {
        generateNMoveVectors();
        generateBMoveVectors();
        generateRMoveVectors();
        generateKMoveVectors();
        generateRookDefenseVectors();
        generateBishopDefenseVectors();
        generateKingExclusionZones();
        generateKingSafetyZones();
        generatePawnAttacks();
    }

    private static void generatePawnAttacks() {
        PAWN_ATTACK = new boolean[2][64][64];
        int x = -7;
        int y = -9;
        for (int side = 0; side < 2; side++) {
            for (int i = 0; i < 64; i++) {
                for (int j = 0; j < 64; j++) {
                    int p = i + x;
                    int q = i + y;
                    if (p == j || q == j) PAWN_ATTACK[side][i][j] = true;
                }
            }
            x = -x;
            y = -y;
        }
    }

    // generate the king exclusion zone, used to prevent kings from approaching each other
    // illegally during move generation
    private static void generateKingExclusionZones() {
        KING_EXCLUSION_ZONE = new long[64];

        KING_EXCLUSION_ZONE[0] = SQUARE_MASK[0] | SQUARE_MASK[1] | SQUARE_MASK[8] | SQUARE_MASK[9];
        KING_EXCLUSION_ZONE[1] = SQUARE_MASK[0] | SQUARE_MASK[1] | SQUARE_MASK[2] | SQUARE_MASK[8] | SQUARE_MASK[9] | SQUARE_MASK[10];

        for (int i = 2; i < 7; i++) {
            KING_EXCLUSION_ZONE[i] = KING_EXCLUSION_ZONE[i - 1] << 1L;
        }

        KING_EXCLUSION_ZONE[7] = SQUARE_MASK[6] | SQUARE_MASK[7] | SQUARE_MASK[14] | SQUARE_MASK[15];

        for (int i = 8; i < 16; i++) {
            KING_EXCLUSION_ZONE[i] = KING_EXCLUSION_ZONE[i - 8] | (KING_EXCLUSION_ZONE[i - 8] << 8L);
        }

        for (int i = 16; i < 56; i++) {
            KING_EXCLUSION_ZONE[i] = KING_EXCLUSION_ZONE[i - 8] << 8L;
        }

        for (int i = 56; i < 64; i++) {
            KING_EXCLUSION_ZONE[i] = KING_EXCLUSION_ZONE[i - 56] << 48L;
        }
    }

    // identifies the three squares in front of a castled king
    // that ordinarily will contain pawns
    private static void generateKingSafetyZones() {
        KING_SAFETY_ZONE = new long[64];
        System.arraycopy(SQUARE_MASK, 16, KING_SAFETY_ZONE, 0, 3);
        System.arraycopy(SQUARE_MASK, 21, KING_SAFETY_ZONE, 5, 3);
        System.arraycopy(SQUARE_MASK, 40, KING_SAFETY_ZONE, 56, 3);
        System.arraycopy(SQUARE_MASK, 45, KING_SAFETY_ZONE, 61, 3);
    }

    // generate the defense vectors (blocking squares) for a rook on sq1 attacking sq2
    private static void generateRookDefenseVectors() {
        for (int i = 0; i < 64; i++) {
            for (int j = 0; j < 64; j++) {
                if (RANK[i] == RANK[j]) {
                    ATTACKER_64[i << 6 | j] = ROOK;
                    if (i < j) {    // attack is along a file from left to right
                        for (int k = i; k < j; k++) {
                            ROOK_DEFENSE_VECTORS_64[i << 6 | j] |= SQUARE_MASK[k];
                        }
                    } else {
                        for (int k = i; k > j; k--) { // attack is along a file from right to left
                            ROOK_DEFENSE_VECTORS_64[i << 6 | j] |= SQUARE_MASK[k];
                        }
                    }
                } else if (FILE[i] == FILE[j]) {
                    ATTACKER_64[i << 6 | j] = ROOK;
                    if (i < j) {    // attack is along a rank from top to bottom (a8 = 0, a1 = 56)
                        for (int k = i; k < j; k += 8) {
                            ROOK_DEFENSE_VECTORS_64[i << 6 | j] |= SQUARE_MASK[k];
                        }
                    } else {
                        for (int k = i; k > j; k -= 8) {
                            ROOK_DEFENSE_VECTORS_64[i << 6 | j] |= SQUARE_MASK[k];
                        }
                    }
                }
            }
        }
    }

    // generate the defense vectors (blocking squares) for a bishop on sq1 attacking sq2
    private static void generateBishopDefenseVectors() {
        for (int i = 0; i < 64; i++) {
            for (int j = 0; j < 64; j++) {
                if ((BISHOP_MOVE_VECTORS_A8_H1_64[i << 8] & SQUARE_MASK[j]) != 0) {
                    ATTACKER_64[i << 6 | j] = BISHOP;
                    if (i < j) {
                        for (int k = i; k < j; k += 9) {
                            BISHOP_DEFENSE_VECTORS_64[i << 6 | j] |= (1L << k);
                        }
                    } else {
                        for (int k = i; k > j; k -= 9) {
                            BISHOP_DEFENSE_VECTORS_64[i << 6 | j] |= (1L << k);
                        }
                    }
                } else if ((BISHOP_MOVE_VECTORS_A1_H8_64[i << 8] & SQUARE_MASK[j]) != 0) {
                    ATTACKER_64[i << 6 | j] = BISHOP;
                    if (i > j) {    // attack is from bottom left to top right
                        for (int k = i; k > j; k -= 7) {
                            BISHOP_DEFENSE_VECTORS_64[i << 6 | j] |= (1L << k);
                        }
                    } else { // attack is from top right to bottom left.
                        for (int k = i; k < j; k += 7) {
                            BISHOP_DEFENSE_VECTORS_64[i << 6 | j] |= (1L << k);
                        }
                    }
                }
            }
        }
    }

    // creates the knight move vectors, the list of all moves for a knight on each square
    // on an empty board.
    private static void generateNMoveVectors() {
        for (int i = 0; i < 64; i++) {
            if (RANK[i] > 0) {
                if (RANK[i] > 1) {
                    if (FILE[i] > 0) {
                        KNIGHT_MOVE_VECTORS[i] += (1L << (i - 17));
                        ATTACKER_64[i << 6 | i - 17] = KNIGHT;
                    }
                    if (FILE[i] < 7) {
                        KNIGHT_MOVE_VECTORS[i] += (1L << (i - 15));
                        ATTACKER_64[i << 6 | i - 15] = KNIGHT;
                    }
                }
                if (FILE[i] > 1) {
                    KNIGHT_MOVE_VECTORS[i] += (1L << (i - 10));
                    ATTACKER_64[i << 6 | i - 10] = KNIGHT;

                }
                if (FILE[i] < 6) {
                    KNIGHT_MOVE_VECTORS[i] += (1L << (i - 6));
                    ATTACKER_64[i << 6 | i - 6] = KNIGHT;
                }
            }
            if (RANK[i] < 7) {
                if (RANK[i] < 6) {
                    if (FILE[i] > 0) {
                        KNIGHT_MOVE_VECTORS[i] += (1L << (i + 15));
                        ATTACKER_64[i << 6 | i + 15] = KNIGHT;
                    }
                    if (FILE[i] < 7) {
                        KNIGHT_MOVE_VECTORS[i] += (1L << (i + 17));
                        ATTACKER_64[i << 6 | i + 17] = KNIGHT;
                    }
                }
                if (FILE[i] > 1) {
                    KNIGHT_MOVE_VECTORS[i] += (1L << (i + 6));
                    ATTACKER_64[i << 6 | i + 6] = KNIGHT;
                }
                if (FILE[i] < 6) {
                    KNIGHT_MOVE_VECTORS[i] += (1L << (i + 10));
                    ATTACKER_64[i << 6 | i + 10] = KNIGHT;
                }
            }
        }
    }

    // generate the bishop move vectors. these are done in two stages, first the
    // a1-h8 diagonal and then the a8-h1 diagonal that every square intersects.
    // because sliding pieces cannot move beyond a blocking piece it is necessary
    // to calculate for each sq every possible combination of occupancy on
    // the trajectory under consideration (up to 64^2 possibilities).
    private static void generateBMoveVectors() {
        // a1-h8 diagonals
        for (int i = 0; i < 64; i++) {
            // Get the far left hand square on this diagonal
            int startSquare = 7 * (Math.min(FILE[i], 7 - RANK[i])) + i;
            int file = FILE[startSquare];
            int dl = VECTOR_LENGTH_A1_H8[i];
            int fi = FILE[i];

            // Loop through all possible occupations of this diagonal line
            for (int j = 0; j < (1 << dl); j++) {
                long mask = 0;
                long mask2 = 0;

                // Calculate possible target squares
                for (int x = (fi - file) - 1; x >= 0; x--) {
                    mask += (1L << x);
                    if ((j & (1 << x)) != 0) break;
                }
                for (int x = (fi - file) + 1; x < dl; x++) {
                    mask += (1L << x);
                    if ((j & (1 << x)) != 0) break;
                }

                // Rotate the target line back onto the required diagonal
                for (int x = 0; x < dl; x++) {
                    mask2 += (((mask >> x) & 1) << (startSquare - (7 * x)));
                }
                BISHOP_MOVE_VECTORS_A1_H8_64[i << 8 | j] = mask2;
            }
        }

        // a8h1 diagonals
        for (int i = 0; i < 64; i++) {
            // Get the far left hand square on this diagonal
            int startSquare = i - 9 * (Math.min(FILE[i], RANK[i]));
            int file = FILE[startSquare];
            int dl = VECTOR_LENGTH_A8_H1[i];
            int fi = FILE[i];

            // Loop through all possible occupations of this diagonal line
            for (int j = 0; j < (1 << dl); j++) {
                long mask = 0;
                long mask2 = 0;

                // Calculate possible target squares
                for (int x = (fi - file) - 1; x >= 0; x--) {
                    mask += (1L << x);
                    if ((j & (1 << x)) != 0) break;
                }
                for (int x = (fi - file) + 1; x < dl; x++) {
                    mask += (1L << x);
                    if ((j & (1 << x)) != 0) break;
                }

                // Rotate target squares back
                for (int x = 0; x < dl; x++) {
                    mask2 += (((mask >> x) & 1) << (startSquare + (9 * x)));
                }
                BISHOP_MOVE_VECTORS_A8_H1_64[i << 8 | j] = mask2;
            }
        }
    }

    // generate the rook move vectors. these are done in two stages, for
    // horizontal moves from a square, and for vertical moves from a square.
    // because sliding pieces cannot move beyond a blocking piece it is necessary
    // to calculate for each sq every possible combination of occupancy on
    // the trajectory under consideration (64^2 possibilities).
    private static void generateRMoveVectors() {
        // calculate the occupancy masks for the file-wise moves
        // for every position (64 * 256)
        for (int file = 0; file < 8; file++) {
            for (int index = 0; index < 256; index++) {
                long occupancyMask = 0;
                for (int sq = file - 1; sq >= 0; sq--) {
                    occupancyMask += (1L << sq);
                    if ((index & (1 << sq)) != 0) break;
                }
                for (int sq = file + 1; sq < 8; sq++) {
                    occupancyMask += (1L << sq);
                    if ((index & (1 << sq)) != 0) break;
                }
                for (int rank = 0; rank < 8; rank++) {
                    ROOK_MOVE_VECTORS_RANK_64[((rank * 8) + file) << 8 | index] = occupancyMask << (rank * 8);
                }
            }
        }

        // calculate the occupancy masks for the rank-wise moves
        // for every position (64 * 256)
        for (int rank = 0; rank < 8; rank++) {
            for (int index = 0; index < 256; index++) {
                long occupancyMask = 0;
                for (int x = 6 - rank; x >= 0; x--) {
                    occupancyMask += (1L << (8 * (7 - x)));
                    if ((index & (1 << x)) != 0) break;
                }
                for (int x = 8 - rank; x < 8; x++) {
                    occupancyMask += (1L << (8 * (7 - x)));
                    if ((index & (1 << x)) != 0) break;
                }
                for (int file = 0; file < 8; file++) {
                    ROOK_MOVE_VECTORS_FILE_64[((rank * 8) + file) << 8 | index] = occupancyMask << file;
                }
            }
        }
    }

    // generate all the moves for a king on every square of the board
    private static void generateKMoveVectors() {
        for (int i = 0; i < 64; i++) {
            if (RANK[i] > 0) {
                if (FILE[i] > 0) KING_MOVE_VECTORS[i] += (1L << (i - 9));
                if (FILE[i] < 7) KING_MOVE_VECTORS[i] += (1L << (i - 7));
                KING_MOVE_VECTORS[i] += (1L << (i - 8));
            }
            if (RANK[i] < 7) {
                if (FILE[i] > 0) KING_MOVE_VECTORS[i] += (1L << (i + 7));
                if (FILE[i] < 7) KING_MOVE_VECTORS[i] += (1L << (i + 9));
                KING_MOVE_VECTORS[i] += (1L << (i + 8));
            }
            if (FILE[i] > 0) KING_MOVE_VECTORS[i] += (1L << (i - 1));
            if (FILE[i] < 7) KING_MOVE_VECTORS[i] += (1L << (i + 1));
        }
    }

}