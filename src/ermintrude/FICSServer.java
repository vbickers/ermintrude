package ermintrude;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.StringTokenizer;

public final class FICSServer {

    public static String loginHandle = "egbertnobacon";
    public static String main_server_address = "www.freechess.org";
    public static int main_server_port = 5000;

    static final String HEADER = "<12>";

    private Socket server;       		/* socket used to communicate with server */
    private DataOutputStream os;
    private DataInputStream is;

    private final Ermintrude computer = new Ermintrude(null, 10);
    private String opponent = "nobody";

    public void talk() {
        new Thread() {
            public void run() {
                try {
                    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
                    //noinspection InfiniteLoopStatement
                    for (;;) {
                        System.out.print("> ");
                        String command = input.readLine();
                        if (command.length() > 0) {
                            command = checkCommand(command);
                            os.writeBytes(command + "\n");
                            if (command.equals("logout")) {
                                closeAndQuit();
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    closeAndQuit();
                }
            }

            private String checkCommand(String command) {
                if (command.indexOf("/") == 0) {
                    command = "tell " + opponent + " " + command.substring(1);
                }
                return command;
            }

        }.start();
    }

    void connect() {
        try {
            server = new Socket(main_server_address, main_server_port);
            os = new DataOutputStream(server.getOutputStream());
            is = new DataInputStream(server.getInputStream());
            login();
            talk();
            listen();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAndQuit();
        }
    }

    private void closeAndQuit() {
        //noinspection finally
        try {
            sendMessage("logout\n");
            OffHeapPositionHashTable.close();
            os.close();
            is.close();
            server.close();
        } catch (Exception e) {
            //e.printStackTrace();
        } finally {
            System.exit(0);
        }
    }

    private void playBlack() {
        computer.newGame();
        computer.playBlack();
        stopMatchRequests();
    }

    private void playWhite() {
        computer.newGame();
        computer.playWhite();
        stopMatchRequests();
        play();
    }

    private void stopMatchRequests() {
        sendMessage("set open=0\n");
    }

    private void play() {
        try {
            int move = computer.playPosition();
            if (move != 0) {
                sendMessage(Utils.format(move) + "\n");
            } else {
                if (computer.isDraw()) {
                    sendMessage("draw\n");    // claim the draw in case the opponent doesn't
                }
                computer.newGame();
            }
        } catch (Exception e) {
            e.printStackTrace();
            closeAndQuit();
        }
    }

    private void listen() {
        try {
            String buffer;
            //noinspection deprecation
            while ((buffer = is.readLine()) != null) {
                StringTokenizer stringTokenizer = new StringTokenizer(buffer);
                if (stringTokenizer.hasMoreTokens()) {
                    String token = stringTokenizer.nextToken();

                    if (token.equals("Creating:")) {
                        sendMessage("set style 12\n");
                        String white = stringTokenizer.nextToken();
                        stringTokenizer.nextToken();
                        String black = stringTokenizer.nextToken();
                        System.out.println(buffer);
                        if (white.equals(loginHandle)) {
                            opponent = black;
                            playWhite();
                        } else {
                            opponent = white;
                            playBlack();
                        }
                        System.out.println("starting game with " + opponent);
                    } else if (token.equals("Issuing:")) {
                        sendMessage("accept\n");
                    } else if (token.equals(HEADER)) {
                        String[] tokens = new String[34];
                        int i = 0;
                        while (stringTokenizer.hasMoreTokens()) {
                            tokens[i++] = stringTokenizer.nextToken();
                        }
                        if (!tokens[26].equals("none")) {
                            String mv = algebraicFormat(tokens[26]);
                            int move = computer.validateMove(mv.toLowerCase());
                            if (move != 0) {
                                System.out.println("move: " + mv);
                                computer.playMove(move);
                                if (((computer.isComputerPlayingWhite()) && computer.isWhiteToMove()) ||
                                        ((computer.isComputerPlayingBlack()) && computer.isBlackToMove())) {
                                    play();
                                }
                            } else {
                                System.out.println("Unexpected parse error:");
                                listTokens(tokens);
                                seekBullet();
                            }
                        } else {
                            int minutes = Integer.parseInt(tokens[19]);
                            int secondsIncrement = Integer.parseInt(tokens[20]);
                            System.out.println("Setting time control to " + minutes + " " + secondsIncrement);
                            computer.setTimeControl(minutes, secondsIncrement);
                        }
                    } else if (buffer.contains(opponent)) {
                        //TODO: catch resign/forfeit/etc.
                        System.out.println(buffer);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Quitting");
            closeAndQuit();
        }
    }

    private void listTokens(String[] tokens) {
        for (int i = 0; i < tokens.length; i++) {
            System.out.println(i + ": " + tokens[i]);
        }
    }

    private String algebraicFormat(String longMove) {
        if (longMove.charAt(0) == 'o') {
            if (computer.isWhiteToMove()) {
                if (longMove.length() == 3) {
                    return "e1g1";
                } else {
                    return "e1c1";
                }
            } else {
                if (longMove.length() == 3) {
                    return "e8g8";
                } else {
                    return "e8c8";
                }
            }
        }
        if (longMove.length() >= 6) {
            return longMove.substring(2, 4) + longMove.substring(5);
        } else {
            return "";
        }
    }

    private void seekBullet() {
        sendMessage("seek 1 0 u 0-9999\n");
        System.out.println("Seeking a bullet game: 1 0 u 0-9999");
    }

    void sendMessage(String str) {
        try {
            os.writeBytes(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void login() throws Exception {
        Thread.sleep(5);
        sendMessage(loginHandle + "\n");
        Thread.sleep(5);
        sendMessage("\n");
        Thread.sleep(1);
        sendMessage("set style 12\n");
        sendMessage("set autoflag on\n");
        System.out.println("*** Logged in to FICS ***");
        seekBullet();
    }

    public static void main(String[] args) {
        FICSServer ficsServer = new FICSServer();
        try {
            ficsServer.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}