package ermintrude;

import static ermintrude.Constants.*;
import static ermintrude.Utils.format;

import java.util.Random;

public final class Ermintrude {

    private static final String[] openingMoves = {"e2e4", "d2d4", "c2c4", "g1f3", "b2b4", "b2b3", "f2f4", "g2g3", "g2g4", "b1c3"};

    private static final int BOTH = WHITE | BLACK;
    private static final int NEITHER = 0;
    private static final int RESIGN = -1;


    private final Book book = new Book();
    private final CLI cli = new CLI();

    private int ermintrude = BLACK;

    private int ply = 0;                // move this onto the game class

    private final boolean useBook;        // move this onto the ermintrude.Game class
    private boolean bookMove = false;    // move this onto the ermintrude.Game class
    private int score = 0;        // move this onto the ermintrude.Game class

    public Ermintrude(String position, int timeout) {
        Engine.initialise();
        Engine.setTimeControl(timeout);
        Board.reset();
        if (position != null) {
            setPosition(position);
            useBook = false;
        } else {
            useBook = true;
            book.open();
        }
        Game.setOpening();
    }

    public void setPosition(String position) {
        FENParser.setPosition(position);
    }

    public void playWhite() {
        ermintrude = WHITE;
    }

    public void playBlack() {
        ermintrude = BLACK;
    }

    public void watch() {
        ermintrude = NONE;
    }

    public int playPosition() {

        int move;
        score = 0;
        bookMove = false;
        Engine.checkmate = false;
        Engine.stalemate = false;

        if (gameOver()) {
            return 0;
        }

        if (ply < 20 && useBook) {
            move = book.pick(FENParser.getPosition());
            if (move != NONE) {
                bookMove = true;
                return move;
            }
        }

        OffHeapPositionHashTable.clear();

        Move mv = Engine.search();

        score = mv.score;
        return mv.move;
    }

    private boolean gameOver() {
        if (Engine.isCheckmate()) {
            System.out.println("Checkmate");
            return true;
        }
        if (Engine.isStalemate()) {
            System.out.println("Stalemate 1/2-1/2");
            return true;
        }
        if (Board.insufficientMaterial()) {
            System.out.println("Draw due to insufficient material 1/2-1/2");
            return true;
        }
        if (Game.isThreeFoldRepetition()) {
            System.out.println("Draw due to threefold repetition 1/2-1/2");
            return true;
        }
        if (Game.isFiftyMoveDraw()) {
            System.out.println("Draw according to the 50-move rule 1/2-1/2");
            return true;
        }
        return false;
    }

    public void playMove(int move) {
        Board.move(move);
        OffHeapPositionHashTable.clear();
        ply++;
    }

    public int validateMove(String mv) {
        return MoveGenerator.validateMove(mv);
    }

    public boolean isDraw() {
        return Engine.isDraw();
    }

    public void play() {
        Engine.initialiseSearch();
        while (!gameOver()) {
            int move;

            if ((Board.SIDE_TO_MOVE & ermintrude) == Board.SIDE_TO_MOVE) {
                if (ply == 0 && Board.SIDE_TO_MOVE == WHITE && useBook) {
                    move = MoveGenerator.validateMove(chooseOpeningMove()); // should choose random opening
                } else {
                    move = playPosition();
                }
            } else {
                // start pondering?
                move = inputMove();
                // stop pondering?
                if (move == RESIGN) {
                    break;
                }
            }
            if (move != NONE) {
                if (bookMove) {
                    System.out.println(Utils.shortFormat(move) + " (" + book.opening(FENParser.getPosition()) + ")");
                } else {
                    System.out.println(Utils.shortFormat(move));
                }
                playMove(move);
                if (score == Engine.CHECKMATE) {
                    System.out.println("Checkmate");
                    break;
                }
            }
        }
        System.out.println("Hashtable writes: " + OffHeapPositionHashTable.writes + ", hits: " + OffHeapPositionHashTable.hits + ", misses: " + OffHeapPositionHashTable.misses);
        OffHeapPositionHashTable.close();
        System.out.println("Goodbye dear heart");
    }

    private String chooseOpeningMove() {
        Random random = new Random();
        bookMove = true;
        return openingMoves[random.nextInt(openingMoves.length)];
    }

    public int inputMove() {
        System.out.println(format(Board.SQUARE_HASH));
        System.out.println();

        for (; ; ) {
            String mv = cli.read();

            if (mv.equals("q")) {
                return RESIGN;    // quit
            }

            if (mv.equals("p")) {
                ermintrude = Board.SIDE_TO_MOVE; // play the side to move
                return NONE;
            }

            if (mv.equals("b")) {
                ermintrude = BOTH;     // play both sides
                return NONE;
            }

            if (mv.equals("w")) {
                ermintrude = NEITHER;  // just watch the game
                return NONE;
            }

            if (mv.equals("n")) {
                newGame();
                return NONE;
            }

            if (mv.equals("f")) {
                System.out.println(FENParser.getPosition());
                return NONE;
            }

            if (mv.equals("e")) {
                LoggingEvaluation.printEvaluation();
                return NONE;
            }

            if (mv.equals("d")) {
                Game.savePGN("?", "?", 0, 0, 0);
                return NONE;
            }

            if (mv.equals("u")) {
                undo();
                return NONE;
            }

            if (mv.equals("dumpKeys")) {
                ZobristHash.dumpKeys();
                return NONE;
            }

            if (mv.equals("loadKeys")) {
                ZobristHash.loadKeys();
                return NONE;
            }

            if (mv.equals("h")) {
                System.out.println("\nEnter a move in the form 'g1f3', or type one of the following commands:");
                System.out.println("\n\th:\t this help");
                System.out.println("\tq:\t quit / resign");
                System.out.println("\tn:\t start a new game");
                System.out.println("\ts:\t show / hide stats");
                System.out.println("\ti:\t show / hide info");
                System.out.println("\te:\t show evaluation");
                System.out.println("\tf:\t display current FEN position");
                System.out.println("\tp:\t tell the computer to play the current position");
                System.out.println("\tb:\t tell the computer to play both sides");
                System.out.println("\tw:\t tell the computer not to play, but just to watch the game");
                System.out.println("\tu:\t undo the last move");

                return NONE;
            } else if (mv.length() > 0) {
                int move = validateMove(mv);
                if (move != 0) return move;
            }
            System.out.println(mv + " is not a valid move in this position, or is not a recognised command.");
            System.out.println("Try again, type 'h' for help or 'q' to quit");
        }
    }

    public void undo() {
        Board.takeBack(Game.lastMove());
        System.out.println(format(Board.SQUARE_HASH));
    }

    public void newGame() {
        ermintrude = NEITHER; // new game
        Board.reset();
        Engine.initialiseSearch();
        ply = 0;
    }

    public int getSideToMove() {
        return Board.SIDE_TO_MOVE;
    }

    public void setTimeControl(int minutesForGame, int secondsIncrement) {
        Engine.setTimeControl(minutesForGame, secondsIncrement);
    }

    public boolean isComputerPlayingWhite() {
        return ermintrude == WHITE;
    }

    public boolean isComputerPlayingBlack() {
        return ermintrude == BLACK;
    }

    public boolean isWhiteToMove() {
        return ply % 2 == 0;
    }

    public boolean isBlackToMove() {
        return ply % 2 == 1;
    }

    public int getScore() {
        return score;
    }

    public static void main(String[] args) {

        try {
            Configuration configuration = new Configuration(args);
            Ermintrude computer = new Ermintrude(configuration.getPosition(), configuration.getSecondsPerMove());
//            int score = Evaluate.evaluate();
//            long hash = Board.HASH_VALUE;
//            OffHeapPositionHashTable.put(hash, 1, score, OffHeapPositionHashTable.EXACT, new Book().parseMove("d4g1", 1));
//            Position p = OffHeapPositionHashTable.get(hash);
//            System.out.println("hash: " + p.hash + " (" + hash + ")");
//            System.out.println("score: " + p.score + " (" + score + ")");
//            System.out.println("flags: " + p.flags + " (" + OffHeapPositionHashTable.EXACT + ")");
//            System.out.println("move: " + format(p.move) + " (d4g1)");
            System.out.println("Max memory: " + Runtime.getRuntime().maxMemory());
            computer.watch();
            computer.play();
        } catch (OutOfMemoryError outOfMemoryError) {
            System.out.println("Not enough JVM memory, can't start Ermintrude");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            OffHeapPositionHashTable.close();
        }
    }
}