package ermintrude;

public final class MoveGenerator {
    static long getKingMoves(int player) {
        long v;
        if (player == Constants.WHITE) {
            v = getKingControlVector(Board.KING_SQUARE_WHITE) & ~(Board.WHITE_MAP | MoveVectors.KING_EXCLUSION_ZONE[Board.KING_SQUARE_BLACK]);
            v |= castleWhite();
        } else {
            v = getKingControlVector(Board.KING_SQUARE_BLACK) & ~(Board.BLACK_MAP | MoveVectors.KING_EXCLUSION_ZONE[Board.KING_SQUARE_WHITE]);
            v |= castleBlack();
        }
        return v;
    }

    static long getKingCaptures(int player) {
        if (player == Constants.WHITE) {
            return getKingControlVector(Board.KING_SQUARE_WHITE) & Board.BLACK_MAP & ~MoveVectors.KING_EXCLUSION_ZONE[Board.KING_SQUARE_BLACK];
        } else {
            return getKingControlVector(Board.KING_SQUARE_BLACK) & Board.WHITE_MAP & ~MoveVectors.KING_EXCLUSION_ZONE[Board.KING_SQUARE_WHITE];
        }
    }

    static long getKingCheckEvasions() {
        if (Board.SIDE_TO_MOVE == Constants.WHITE) {
            long v = getKingControlVector(Board.KING_SQUARE_WHITE) & ~(Board.WHITE_MAP | MoveVectors.KING_EXCLUSION_ZONE[Board.KING_SQUARE_BLACK]);
            return v & ~getControlledSquares(Constants.BLACK);
        } else {
            long v = getKingControlVector(Board.KING_SQUARE_BLACK) & ~(Board.BLACK_MAP | MoveVectors.KING_EXCLUSION_ZONE[Board.KING_SQUARE_WHITE]);
            return v & ~getControlledSquares(Constants.WHITE);
        }
    }

    static long getQueenMoves(int sq, int player) {
        long playerMap = (player == Constants.WHITE ? Board.WHITE_MAP : Board.BLACK_MAP);
        return getQueenControlVector(sq) & ~playerMap;
    }

    static long getQueenCaptures(int sq, int player) {
        long opponentMap = (player == Constants.WHITE ? Board.BLACK_MAP : Board.WHITE_MAP);
        return getQueenControlVector(sq) & opponentMap;
    }

    static long getQueenCheckEvasions(int sq) {
        return Board.CHECK_VECTOR & getQueenControlVector(sq);
    }

    static long getRookMoves(int sq, int player) {
        long playerMap = (player == Constants.WHITE ? Board.WHITE_MAP : Board.BLACK_MAP);
        return getRookControlVector(sq) & ~playerMap;
    }

    static long getRookCaptures(int sq, int player) {
        long opponentMap = (player == Constants.WHITE ? Board.BLACK_MAP : Board.WHITE_MAP);
        return getRookControlVector(sq) & opponentMap;
    }

    static long getRookCheckEvasions(int sq) {
        return Board.CHECK_VECTOR & getRookControlVector(sq);
    }

    static long getBishopMoves(int sq, int player) {
        long playerMap = (player == Constants.WHITE ? Board.WHITE_MAP : Board.BLACK_MAP);
        return getBishopControlVector(sq) & ~playerMap;
    }

    static long getBishopCaptures(int sq, int player) {
        long opponentMap = (player == Constants.WHITE ? Board.BLACK_MAP : Board.WHITE_MAP);
        return getBishopControlVector(sq) & opponentMap;
    }

    static long getBishopCheckEvasions(int sq) {
        return Board.CHECK_VECTOR & getBishopControlVector(sq);
    }

    static long getKnightMoves(int sq, int player) {
        long playerMap = (player == Constants.WHITE ? Board.WHITE_MAP : Board.BLACK_MAP);
        return MoveVectors.KNIGHT_MOVE_VECTORS[sq] & ~playerMap;
    }

    static long getKnightCaptures(int sq, int player) {
        long opponentMap = (player == Constants.WHITE ? Board.BLACK_MAP : Board.WHITE_MAP);
        return MoveVectors.KNIGHT_MOVE_VECTORS[sq] & opponentMap;
    }

    static long getKnightCheckEvasions(int sq) {
        return Board.CHECK_VECTOR & getKnightControlVector(sq);
    }

    static long getPawnMoves(int sq) {
        int player = Board.SIDE_TO_MOVE;
        long opponentMap;
        long c = 0;
        long m = 0;
        long p = (1L << sq);
        long epBit = Board.EN_PASSANT_SQUARE == -1 ? 0 : Constants.SQUARE_MASK[Board.EN_PASSANT_SQUARE];

        if (player == Constants.WHITE) {
            opponentMap = Board.BLACK_MAP;
            c |= (p >> 9) & (~Constants.H_FILE);
            c |= (p >> 7) & (~Constants.A_FILE);
            m |= (p >> 8) & (~Board.BOARD_MAP);
            if (m != 0) m |= ((p & 0x00FF000000000000L) >> 16) & (~Board.BOARD_MAP);
        } else {
            opponentMap = Board.WHITE_MAP;
            c |= (p << 7) & (~Constants.H_FILE);
            c |= (p << 9) & (~Constants.A_FILE);
            m |= (p << 8) & (~Board.BOARD_MAP);
            if (m != 0) m |= ((p & 0x000000000000FF00L) << 16) & (~Board.BOARD_MAP);
        }
        return (c & opponentMap) | (c & epBit) | m;
    }

    static long getPawnCaptures(int sq) {
        int player = Board.SIDE_TO_MOVE;
        long opponentMap;
        long c = 0;
        long p = (1L << sq);
        long epBit = Board.EN_PASSANT_SQUARE == -1 ? 0 : Constants.SQUARE_MASK[Board.EN_PASSANT_SQUARE];

        if (player == Constants.WHITE) {
            opponentMap = Board.BLACK_MAP;
            c |= (p >> 9) & (~Constants.H_FILE);
            c |= (p >> 7) & (~Constants.A_FILE);
        } else {
            opponentMap = Board.WHITE_MAP;
            c |= (p << 7) & (~Constants.H_FILE);
            c |= (p << 9) & (~Constants.A_FILE);
        }
        return (c & opponentMap) | (c & epBit);
    }

    static long getPawnCheckEvasions(int sq) {
        return getPawnDefenceVector(sq);
    }

    /*
     * Generates a vector representing all the squares controlled by the side 'side'.
     *
     * A square 't' is controlled by a piece on square 'f' if the piece's capture trajectory includes 't'.
     * This is irrespective of whether the square is actually occupied or not, and if occupied, the
     * colour of the piece on square 't' is also of no consequence. The other point of relevance
     * is that the location of the opponent's king must be ignored - i.e. sliding pieces have
     * 'xray' control through the opponent's king . Thus, in a position with one side in check
     * by say a rook, the rook's influence extends through the king along the line of check, not merely up
     * to the king. So, here, and here only, we turn the opponent king bit off in the map. We must
     * ensure we put it back again before we exit, or before any other method wants to use the maps, as
     * it is inherently unsafe.
     */
    static long getControlledSquares(int side) {
        long v = 0, pawnMap, pieceMap;
        int ksq = Board.KING_SQUARE_WHITE;

        if (side == Constants.WHITE) ksq = Board.KING_SQUARE_BLACK;

        long rotR90 = Constants.SQUARE_MASK_R90[ksq];
        long rotR45 = Constants.SQUARE_MASK_R45[ksq];
        long rotL45 = Constants.SQUARE_MASK_L45[ksq];

        Board.BOARD_MAP ^= Constants.SQUARE_MASK[ksq];
        Board.ROTATED_RIGHT_90 ^= rotR90;
        Board.ROTATED_RIGHT_45 ^= rotR45;
        Board.ROTATED_LEFT_45 ^= rotL45;

        // set up the pawn bitmap
        pawnMap = (side == Constants.WHITE) ? Board.WHITE_PAWNS : Board.BLACK_PAWNS;
        pieceMap = (side == Constants.WHITE) ? Board.WHITE_MAP ^ Board.WHITE_PAWNS : Board.BLACK_MAP ^ Board.BLACK_PAWNS;

        while (pieceMap != 0) {
            int sq = Bits.lsbValue(pieceMap);
            int pieceNameIdentity = Board.SQUARE_HASH[sq];
            pieceNameIdentity -= (pieceNameIdentity > 5 ? 5 : -1);

            if (pieceNameIdentity == Constants.KNIGHT) v |= getKnightControlVector(sq);
            else if (pieceNameIdentity == Constants.BISHOP) v |= getBishopControlVector(sq);
            else if (pieceNameIdentity == Constants.ROOK) v |= getRookControlVector(sq);
            else if (pieceNameIdentity == Constants.QUEEN) v |= getQueenControlVector(sq);
            else if (pieceNameIdentity == Constants.KING) v |= getKingControlVector(sq);

            pieceMap &= pieceMap-1;
        }

        // restore the opponent's king, pawn control vectors cannot xray
        Board.BOARD_MAP ^= Constants.SQUARE_MASK[ksq];
        Board.ROTATED_RIGHT_90 ^= rotR90;
        Board.ROTATED_RIGHT_45 ^= rotR45;
        Board.ROTATED_LEFT_45 ^= rotL45;

        v |= getPawnControlVector(side, pawnMap);

        return v;
    }

    /*
     * Generates the control vectors for the pawn vector 'p' and the side 'side' - i.e. all their potential captures
     */
    private static long getPawnControlVector(int side, long p) {
        long v = 0;
        if (side == Constants.WHITE) {
            v |= (p >> 9) & (~Constants.H_FILE);
            v |= (p >> 7) & (~Constants.A_FILE);
        } else {
            v |= (p << 7) & (~Constants.H_FILE);
            v |= (p << 9) & (~Constants.A_FILE);
        }
        return v;
    }

    /*
     * The list of all pawn moves from square side, for the pawn vector p. This is only used during check evasion
     * The defense vector for a pawn is to either capture the attacking piece, or to block the attack.
     * Since the pawn's movement is not the same in both cases (it can't move forward to capture, and it can't move
     * sideways to block), both the attacker's square and the attack vector have to be known.
     */
    private static long getPawnDefenceVector(int sq) {
        int player = Board.SIDE_TO_MOVE;
        long b = 0, c = 0;
        long p = (1L << sq);
        long epBit = Board.EN_PASSANT_SQUARE == -1 ? 0 : Constants.SQUARE_MASK[Board.EN_PASSANT_SQUARE];

        long blockVector = Board.ATTACKER_MASK ^ Board.CHECK_VECTOR;
        if (player == Constants.WHITE) {
            // handle the capture
            c |= (p >> 9) & (~Constants.H_FILE);
            c |= (p >> 7) & (~Constants.A_FILE);
            c &= (Board.ATTACKER_MASK | epBit);
            if (blockVector != 0)    // it's an attack by a sliding piece. note, the pawn cannot move if it is blocked
            {
                long m = (p >> 8) & ~Board.BOARD_MAP;
                if (m != 0) {
                    b = (m | ((p & 0x00FF000000000000L) >> 16)) & blockVector;
                }
            }
        } else {
            c |= (p << 7) & (~Constants.H_FILE);
            c |= (p << 9) & (~Constants.A_FILE);
            c &= Board.ATTACKER_MASK | epBit;
            if (blockVector != 0) {
                long m = (p << 8) & ~Board.BOARD_MAP;
                if (m != 0) {
                    b |= (m | ((p & 0x000000000000FF00L) << 16)) & blockVector;
                }
            }
        }
        return b | c;
    }

    /*
     * The list of squares controlled by a knight on sq - all its potential moves in other words
     */
    private static long getKnightControlVector(int sq) {
        return MoveVectors.KNIGHT_MOVE_VECTORS[sq];
    }

    /*
     * The list of squares controlled by a bishop on sq - all its potential moves in other words
     */
    static long getBishopControlVector(int sq) {
        int offset = sq << 8;
        int occupancyMaskA1H8 = (int) ((Board.ROTATED_RIGHT_45 >> Constants.VECTOR_SHIFTS_A1_H8[sq]) & Constants.VECTOR_MASK_A1_H8[sq]);
        long v = MoveVectors.BISHOP_MOVE_VECTORS_A1_H8_64[offset | occupancyMaskA1H8];
        int occupancyMaskA8H1 = (int) ((Board.ROTATED_LEFT_45 >> Constants.VECTOR_SHIFTS_A8_H1[sq]) & Constants.VECTOR_MASK_A8_H1[sq]);
        return v | MoveVectors.BISHOP_MOVE_VECTORS_A8_H1_64[offset | occupancyMaskA8H1];
    }

    /*
     * The list of squares controlled by a rook on sq - all its potential moves in other words
     */
    private static long getRookControlVector(int sq) {
        int offset = sq << 8;
        int occupancyMaskRank = (int) (Board.BOARD_MAP >> (Constants.RANK[sq] << 3L)) & 0xFF;
        int occupancyMaskFile = (int) (Board.ROTATED_RIGHT_90 >> (Constants.FILE[sq] << 3L)) & 0xFF;
        long v = MoveVectors.ROOK_MOVE_VECTORS_RANK_64[offset | occupancyMaskRank];
        return v | MoveVectors.ROOK_MOVE_VECTORS_FILE_64[offset | occupancyMaskFile];
    }

    /*
     * The list of squares controlled by a queen on sq - all its potential moves in other words
     */
    private static long getQueenControlVector(int sq) {
        int occupancyMask = (int) (Board.BOARD_MAP >> (Constants.RANK[sq] << 3L)) & 0xFF;
        int offset = sq << 8;
        long v = MoveVectors.ROOK_MOVE_VECTORS_RANK_64[offset | occupancyMask];

        occupancyMask = (int) (Board.ROTATED_RIGHT_90 >> (Constants.FILE[sq] << 3L)) & 0xFF;
        v |= MoveVectors.ROOK_MOVE_VECTORS_FILE_64[offset | occupancyMask];

        occupancyMask = (int) ((Board.ROTATED_RIGHT_45 >> Constants.VECTOR_SHIFTS_A1_H8[sq]) & Constants.VECTOR_MASK_A1_H8[sq]);
        v |= MoveVectors.BISHOP_MOVE_VECTORS_A1_H8_64[offset | occupancyMask];
        occupancyMask = (int) ((Board.ROTATED_LEFT_45 >> Constants.VECTOR_SHIFTS_A8_H1[sq]) & Constants.VECTOR_MASK_A8_H1[sq]);
        return v | MoveVectors.BISHOP_MOVE_VECTORS_A8_H1_64[offset | occupancyMask];
    }

    /*
     * The list of squares controlled by a king on sq - all its potential moves in other words
     */
    private static long getKingControlVector(int sq) {
        return MoveVectors.KING_MOVE_VECTORS[sq];
    }

    // determines whether an attack vector f->t is a legal attack
    static boolean isLegalAttack(int f, int t) {
        int player = Board.SIDE_TO_MOVE;
        int boardAttacker = Board.SQUARE_HASH[f];
        boardAttacker -= (boardAttacker > 5 ? 5 : -1);

        int legalAttacker = MoveVectors.ATTACKER_64[f << 6 | t];
        if (legalAttacker == Constants.BISHOP) {
            if (boardAttacker == legalAttacker || boardAttacker == Constants.QUEEN) {
                return true;
            } else if (boardAttacker == Constants.PAWN) {
                boolean[][] pawnAttack = (player == Constants.WHITE ? MoveVectors.PAWN_ATTACK[1] : MoveVectors.PAWN_ATTACK[0]);
                return pawnAttack[f][t];
            }
        } else if (legalAttacker == Constants.ROOK) {
            return boardAttacker == legalAttacker || boardAttacker == Constants.QUEEN;
        } else return legalAttacker == Constants.KNIGHT && legalAttacker == boardAttacker;
        return false;
    }

    /*
     * returns the attack vector of a piece on 'f' attacking
     * a target on 't'. the attack vector is a trajectory from
     * f to t, but not including t. Thus, for knights, kings
     * and pawns the attack vector is simply the square 'f'.
     */
    static long getAttackVector(int f, int t) {
        int mask = f << 6 | t;
        int type = MoveVectors.ATTACKER_64[mask];
        if (type == Constants.BISHOP) return MoveVectors.BISHOP_DEFENSE_VECTORS_64[mask];
        if (type == Constants.ROOK) return MoveVectors.ROOK_DEFENSE_VECTORS_64[mask];
        return Constants.SQUARE_MASK[f];
    }

    static boolean isKingAttacked() {
        int ksq;
        long attackers;
        if (Board.SIDE_TO_MOVE == Constants.WHITE) {
            ksq = Board.KING_SQUARE_WHITE;
            attackers = Board.getAttacksTo(ksq);
            attackers &= Board.BLACK_MAP;
        } else {
            ksq = Board.KING_SQUARE_BLACK;
            attackers = Board.getAttacksTo(ksq);
            attackers &= Board.WHITE_MAP;
        }
        while (attackers != 0) {
            int t = Bits.lsbValue(attackers);
            if (isLegalAttack(t, ksq)) {
                return true;
            }
            attackers &= attackers - 1;
        }
        return false;
    }

    static int validateMove(String mv) {
        MoveList moveList = new MoveList();
        moveList.getMoves(Board.countChecks(), 1,0);
        int move;
        while ((move = moveList.pick()) != 0) {
            if (mv.equals(Utils.format(move))) {
                return move;
            }
        }
        return 0;
    }

    // get the castle moves for white
    private static long castleWhite() {
        long moves = 0;
        if (Board.CASTLED[Constants.WHITE] == 0 && Board.MOVED[Constants.e1] == 0) {
            if (Board.countAttacks(Constants.e1) == 0) {
                if (Board.MOVED[Constants.h1] == 0) { // king side rook hasn't moved
                    if ((Board.BOARD_MAP & Board.CASTLE_MASK[Constants.WHITE][Constants.KING_SIDE]) == 0) {    // no pieces blocking king side
                        if ((Board.countAttacks(Constants.f1) == 0) && (Board.countAttacks(Constants.g1) == 0)) {
                            moves |= Constants.SQUARE_MASK[Constants.g1];
                        }
                    }
                }
                if (Board.MOVED[Constants.a1] == 0) {// queen side rook hasn't moved
                    if ((Board.BOARD_MAP & Board.CASTLE_MASK[Constants.WHITE][Constants.QUEEN_SIDE]) == 0) {    // no pieces blocking queen side
                        if ((Board.countAttacks(Constants.d1) == 0) && (Board.countAttacks(Constants.c1) == 0)) {
                            moves |= Constants.SQUARE_MASK[Constants.c1];
                        }
                    }
                }
            }
        }
        return moves;
    }

    // get the castle moves for black
    private static long castleBlack() {
        long moves = 0;
        if (Board.CASTLED[Constants.BLACK] == 0 && Board.MOVED[Constants.e8] == 0) {
            if (Board.countAttacks(Constants.e8) == 0) {
                if (Board.MOVED[Constants.h8] == 0) {    // king side rook hasn't moved
                    if ((Board.BOARD_MAP & Board.CASTLE_MASK[Constants.BLACK][Constants.KING_SIDE]) == 0) {    // no pieces blocking king side
                        if ((Board.countAttacks(Constants.f8) == 0) && (Board.countAttacks(Constants.g8) == 0)) {
                            moves |= Constants.SQUARE_MASK[Constants.g8];
                        }
                    }
                }
                if (Board.MOVED[Constants.a8] == 0) {    // queen side rook hasn't moved
                    if ((Board.BOARD_MAP & Board.CASTLE_MASK[Constants.BLACK][Constants.QUEEN_SIDE]) == 0) {    // no pieces blocking queen side
                        if ((Board.countAttacks(Constants.d8) == 0) && (Board.countAttacks(Constants.c8) == 0)) {
                            moves |= Constants.SQUARE_MASK[Constants.c8];
                        }
                    }
                }
            }
        }
        return moves;
    }
}
