# Ermintrude: a Java Chess Engine 
#
Version	description

1.0		first version. The basic java engine: PVS search with evaluation based 
		on material balance only. Single transposition table, used by main search 
		and q-search. no null-moves or forward pruning. also no killers or
		history. board representation is bitboard-based.
		
1.1		added static move scoring during move generation and pick() function 
		to select moves according to their static score, to pick "good" moves 
		first. captures ordered by MVV/LVA. promotions scored as value of
		promoting piece. hash move always played first. removed use of 
		transposition tables in q-search as it didn't actually help and 
		only slowed the search down.

1.2		added piece-square position tables and incorporated them into the evaluation 
		function. piece-square evaluation is computed dynamically as the board 
		position changes. still no proper evaluation function. obvious things missing
		are king-safety and pawn structure. 

1.3		added hash move search before generating any moves in main search. this 
		often causes an immediate cutoff, and even if it does not, the updated 
		alpha and beta values help to prune the subsequent search. 

1.4		added end-game recognisers for KQK, KRK, KBBK and KBNK. now these end-games 
		are always won, directed simply by the heuristics.

1.5		set the default transposition size to 8MB. Added chaining to transposition 
		table to handle collisions.
		
1.6		added simple king evaluation to penalise king if it moves before castling. 
		this is computed dynamically at present. but could be detected automatically 
		during move generation, e.g. if the king moves and it hasn't castled 
		set a "can't castle" penalty. similarly, if a rook moves set the value 
		to half the king penalty.
		
1.7		q-nodes now pruned if eval score + captured piece < alpha. fixed bug in 
		king move generation when adding castling moves
		
1.8		fixed a search bug when alpha and beta failed low, and timeout during research 
		caused the best move at the root to become corrupted. changed the aspiration 
		search window from 75 to 37, for 5-10% speed improvement.

1.9		added positional evaluation for doubled pawns. this is the start
		of the full pawn evaluation. pawn scores are hashed for fast retrieval
		as the pawn structure does not change very quickly. finally implemented 
		futility pruning at frontier nodes

1.10	added killer moves

1.11	removed hash move search prior to generating moves as the hash move was
		not always correct and this could generate bugs. Added razoring for 
		boring-looking moves. Added null-move search and singular reductions. 
		Merged full and q-search. q-search now uses transposition tables.
		
1.12	added an interface to ICC and proper time-management controls. ermintrude
		can now play on the internet.
		
1.13	added full futility pruning, and removed previous razoring code. Restored 
        the hash move search after fixing bugs. set the search window to 60 as 
        37 proved too low, and ermintrude was missing some tactics. ermintrude
        now has an ICC rating of around 2200. Wrote a perft utility and discovered
        that ermintrude is much slower than sjeng or crafty at generating and playing 
        moves. this, as well as poor king safety and lack of understanding of drawn
        endgames is preventing her from obtaining a higher rating. She loses
        many games now due to serious positional blunders, the worst of which is 
        her tendency to push pawns in front of her castled king. 
        
1.14	removed futility pruning entirely and replaced with late move reduction pruning. 
		code is now simpler and faster. 

1.15	added more terms to pawn evaluation: doubled, isolated, backward and passed pawns
		now considered. still need to handle weak pawn structures around king.
        
1.16	now storing q-moves in hash table.

1.17    replaced aspiration search with zero-search window for non PV nodes. Null moves allowed
        anywhere, except after a null move at the previous ply. Minor tweaks to King safety 
        evaluation. Estimated rating 2300-2350.